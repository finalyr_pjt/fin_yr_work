const s3Obj =  require("../s3Config");
const multerS3 = require("multer-s3");
const multer = require('multer');

const middlewareObj = {};


middlewareObj.upload = multer({
        storage: multerS3({
          s3: s3Obj.s3,
          bucket: process.env.AWS_BUCKET_NAME,
          metadata: function (req, file, cb) {
            cb(null, {fieldName: file.fieldname});
          },
          key: function (req, file, cb) {
            cb(null, Date.now().toString()+"-"+file.originalname);
          }
        })
      })


module.exports = middlewareObj;