drop table if exists ongoing_projects;
drop table if exists project;
drop table if exists tester;
drop table if exists developer;

create table tester (
	tester_id serial primary key,
	name varchar(50),
	email varchar(50),
	password varchar(50),
	gender varchar(10),
	date_of_birth date,
	profile_pic varchar(200),
	profile_creation_time timestamp default current_timestamp,
-- 	profile_creation_date date current_date,
	reward_points int,
	block_count int,
	exist varchar(5)
);

create table developer(
	developer_id serial primary key,
	name varchar(50),
	email varchar(50),
	password varchar(50),
	developer_type varchar(50),
	pic_url varchar(200),
	profile_created_on timestamp default current_timestamp,
	exist varchar(5)
);

create table project (
	project_id serial primary key,
	title varchar(50),
	testing_submission_before date,
	enrollment_ends_on date,
	issue_upload_ss varchar(50),
	point_rewards int,
	age_upper_limit int,
	age_lower_limit int,
	about_task varchar(50),
	status varchar(50),
	pages varchar(50),
	developer_id int,
	CONSTRAINT developer_id
      FOREIGN KEY(developer_id) 
	  REFERENCES developer(developer_id)
);

create table ongoing_projects(
	tester_id int,
	constraint tester_id
		foreign key (tester_id)
			references tester(tester_id),
	project_id int,
	constraint project_id
		foreign key (project_id)
			references project(project_id),
	enrolled_on time
);


insert into tester (name,email,password,gender,date_of_birth,profile_pic,reward_points,block_count, exist) values ('Test_Tester' , 'Test_Tester@gamil.com' ,'password','Male' ,'2022-01-10' ,'not_defined_yet',0,0,'yes');
insert into developer (name,email,password,developer_type,pic_url,exist) values ('Test_developer' , 'Test_developer@gmail.com','password','company','not_defined_yed','yes');
insert into project (title,testing_submission_before,enrollment_ends_on,issue_upload_ss,point_rewards,age_upper_limit,age_lower_limit,about_task,status,pages,developer_id) 
	values ('Test_projects' , '2022-02-25','2022-02-15','link  here',100,15,100,'description of task','about status','about pages',1)


