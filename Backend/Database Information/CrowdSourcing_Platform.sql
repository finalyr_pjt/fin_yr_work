PGDMP     "    2    	    
         z            CrowdSourcing_Platform    13.3    13.3     �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    24856    CrowdSourcing_Platform    DATABASE     t   CREATE DATABASE "CrowdSourcing_Platform" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'English_India.1252';
 (   DROP DATABASE "CrowdSourcing_Platform";
                postgres    false            �            1259    25797 	   developer    TABLE     f  CREATE TABLE public.developer (
    developer_id integer NOT NULL,
    name character varying(50),
    email character varying(50),
    password character varying(50),
    developer_type character varying(50),
    pic_url character varying(200),
    profile_created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    exist character varying(5)
);
    DROP TABLE public.developer;
       public         heap    postgres    false            �            1259    25795    developer_developer_id_seq    SEQUENCE     �   CREATE SEQUENCE public.developer_developer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.developer_developer_id_seq;
       public          postgres    false    203            �           0    0    developer_developer_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.developer_developer_id_seq OWNED BY public.developer.developer_id;
          public          postgres    false    202            �            1259    25817    ongoing_projects    TABLE     �   CREATE TABLE public.ongoing_projects (
    tester_id integer,
    project_id integer,
    enrolled_on time without time zone
);
 $   DROP TABLE public.ongoing_projects;
       public         heap    postgres    false            �            1259    25806    project    TABLE     �  CREATE TABLE public.project (
    project_id integer NOT NULL,
    title character varying(50),
    testing_submission_before date,
    enrollment_ends_on date,
    issue_upload_ss character varying(50),
    point_rewards integer,
    age_upper_limit integer,
    age_lower_limit integer,
    about_task character varying(50),
    status character varying(50),
    pages character varying(50),
    developer_id integer
);
    DROP TABLE public.project;
       public         heap    postgres    false            �            1259    25804    project_project_id_seq    SEQUENCE     �   CREATE SEQUENCE public.project_project_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.project_project_id_seq;
       public          postgres    false    205            �           0    0    project_project_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.project_project_id_seq OWNED BY public.project.project_id;
          public          postgres    false    204            �            1259    25788    tester    TABLE     �  CREATE TABLE public.tester (
    tester_id integer NOT NULL,
    name character varying(50),
    email character varying(50),
    password character varying(50),
    gender character varying(10),
    date_of_birth date,
    profile_pic character varying(200),
    profile_creation_time timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    reward_points integer,
    block_count integer,
    exist character varying(5)
);
    DROP TABLE public.tester;
       public         heap    postgres    false            �            1259    25786    tester_tester_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tester_tester_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.tester_tester_id_seq;
       public          postgres    false    201            �           0    0    tester_tester_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.tester_tester_id_seq OWNED BY public.tester.tester_id;
          public          postgres    false    200            4           2604    25800    developer developer_id    DEFAULT     �   ALTER TABLE ONLY public.developer ALTER COLUMN developer_id SET DEFAULT nextval('public.developer_developer_id_seq'::regclass);
 E   ALTER TABLE public.developer ALTER COLUMN developer_id DROP DEFAULT;
       public          postgres    false    202    203    203            6           2604    25809    project project_id    DEFAULT     x   ALTER TABLE ONLY public.project ALTER COLUMN project_id SET DEFAULT nextval('public.project_project_id_seq'::regclass);
 A   ALTER TABLE public.project ALTER COLUMN project_id DROP DEFAULT;
       public          postgres    false    204    205    205            2           2604    25791    tester tester_id    DEFAULT     t   ALTER TABLE ONLY public.tester ALTER COLUMN tester_id SET DEFAULT nextval('public.tester_tester_id_seq'::regclass);
 ?   ALTER TABLE public.tester ALTER COLUMN tester_id DROP DEFAULT;
       public          postgres    false    201    200    201            �          0    25797 	   developer 
   TABLE DATA           |   COPY public.developer (developer_id, name, email, password, developer_type, pic_url, profile_created_on, exist) FROM stdin;
    public          postgres    false    203   C%       �          0    25817    ongoing_projects 
   TABLE DATA           N   COPY public.ongoing_projects (tester_id, project_id, enrolled_on) FROM stdin;
    public          postgres    false    206   �%       �          0    25806    project 
   TABLE DATA           �   COPY public.project (project_id, title, testing_submission_before, enrollment_ends_on, issue_upload_ss, point_rewards, age_upper_limit, age_lower_limit, about_task, status, pages, developer_id) FROM stdin;
    public          postgres    false    205   �%       �          0    25788    tester 
   TABLE DATA           �   COPY public.tester (tester_id, name, email, password, gender, date_of_birth, profile_pic, profile_creation_time, reward_points, block_count, exist) FROM stdin;
    public          postgres    false    201   E&       �           0    0    developer_developer_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.developer_developer_id_seq', 1, true);
          public          postgres    false    202            �           0    0    project_project_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.project_project_id_seq', 1, true);
          public          postgres    false    204            �           0    0    tester_tester_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.tester_tester_id_seq', 1, true);
          public          postgres    false    200            :           2606    25803    developer developer_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.developer
    ADD CONSTRAINT developer_pkey PRIMARY KEY (developer_id);
 B   ALTER TABLE ONLY public.developer DROP CONSTRAINT developer_pkey;
       public            postgres    false    203            <           2606    25811    project project_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_pkey PRIMARY KEY (project_id);
 >   ALTER TABLE ONLY public.project DROP CONSTRAINT project_pkey;
       public            postgres    false    205            8           2606    25794    tester tester_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.tester
    ADD CONSTRAINT tester_pkey PRIMARY KEY (tester_id);
 <   ALTER TABLE ONLY public.tester DROP CONSTRAINT tester_pkey;
       public            postgres    false    201            =           2606    25812    project developer_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.project
    ADD CONSTRAINT developer_id FOREIGN KEY (developer_id) REFERENCES public.developer(developer_id);
 >   ALTER TABLE ONLY public.project DROP CONSTRAINT developer_id;
       public          postgres    false    203    205    2874            ?           2606    25825    ongoing_projects project_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.ongoing_projects
    ADD CONSTRAINT project_id FOREIGN KEY (project_id) REFERENCES public.project(project_id);
 E   ALTER TABLE ONLY public.ongoing_projects DROP CONSTRAINT project_id;
       public          postgres    false    205    206    2876            >           2606    25820    ongoing_projects tester_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.ongoing_projects
    ADD CONSTRAINT tester_id FOREIGN KEY (tester_id) REFERENCES public.tester(tester_id);
 D   ALTER TABLE ONLY public.ongoing_projects DROP CONSTRAINT tester_id;
       public          postgres    false    2872    206    201            �   e   x�3�I-.�OI-K��/H-B�:��&f��%��r$���p9�y��y� �i�y�)�)�FFF����
�V&@d�gldjhb�Y�Z����� ��#U      �      x������ � �      �   `   x�=�]
� ���)vCݤ� �VY����+������' GdYj�-�1�W�<���2�X4�yAr@�(��/JXY���1��� b      �   d   x�3�I-.��E�l��������\΂������N�ĜTN###]C]Cμ����Դ̼Ԕ���$)K+ 2�362541�4 ���b�=... �!�     