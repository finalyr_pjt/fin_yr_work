const LocalStrategy = require("passport-local").Strategy;
const { pool } = require("./db");
const bcrypt = require("bcrypt");
const passportJWT = require("passport-jwt");
const JWTStrategy   = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
fs = require('fs');

// function SessionConstructor(userId, userGroup) {
//     this.userId = userId;
//     this.userGroup = userGroup;
//   }

function initialize(passport){
    const authenticateUser = async(email,password,done)=>{
        try {

            const search = await pool.query(
                "SELECT * FROM TESTER WHERE EMAIL=$1 AND ACTIVE=1",
                [email]);
            
            
                
                // console.log(search.rows);
                if(search.rows.length>0)
                {
                    const user = search.rows[0];
                        bcrypt.compare(password,user.password,(err,isMatch)=>{
                            if(err){
                                
                                console.error(err.message);
                            }
                            if(isMatch){
                                
                                return done(null,user);
                            }
                            else{
                                
                                return done(null,false,{message:"Password is not correct"})
                            }
                        })
                    }
                
                    else {
                 
                                console.log("err mail");
                                return done(null,false,{message:"Email not registered"});
                            }

        } catch (error) {
            console.log(error);
        }
        
        
    };

    passport.use(
        'user',
        new LocalStrategy(
            {
                usernameField: "email",
                passwordField: "password"
            },
            authenticateUser
        )
    );

    const authenticateUserdev = async(email,password,done)=>{
        try {

            const search = await pool.query(
                "SELECT * FROM DEVELOPER WHERE EMAIL=$1 AND ACTIVE=1",
                [email]);
                // console.log(search.rows);
                if(search.rows.length>0)
                {
                    const user = search.rows[0];
                        bcrypt.compare(password,user.password,(err,isMatch)=>{
                            if(err){
                                
                                console.error(err.message);
                            }
                            if(isMatch){
                                
                                return done(null,user);
                            }
                            else{
                                
                                return done(null,false,{message:"Password is not correct"})
                            }
                        })
                    }
                    else{
                                console.log("err mail");
                                return done(null,false,{message:"Email not registered"});
                            }

        } catch (error) {
            console.log(error);
        }
        
        
    };

    passport.use(
        'developer',
        new LocalStrategy(
            {
                usernameField: "email",
                passwordField: "password"
            },
            authenticateUserdev
        )
    );


    passport.use('iftesterAuthenticated',new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey   : fs.readFileSync('private.pem','utf-8')
    },
    async(jwtPayload, cb)=>{

        //find the user in db if needed. This functionality may be omitted if you store everything you'll need in JWT payload.
            await pool.query(
                "SELECT * FROM TESTER WHERE tester_id = $1 AND ACTIVE=1",[jwtPayload.tester_id],(err,results)=>{
                    if(err){
                       
                        return cb(null,results.rows[0]);
                            
                    }
                    return cb(null,results.rows[0]);
                }
            )
            }
            
            
));

    passport.use('ifdevloperAuthenticated',new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey   : fs.readFileSync('private.pem','utf-8')
    },
    async(jwtPayload, cb)=>{

        //find the user in db if needed. This functionality may be omitted if you store everything you'll need in JWT payload.
            await pool.query(
                "SELECT * FROM DEVELOPER WHERE developer_id = $1 AND ACTIVE=1",[jwtPayload.developer_id],(err,results)=>{
                    if(err){
                    
                        
                        return cb(null,results.rows[0]);
                        
                            
                    }
                    return cb(null,results.rows[0]);
                }
            )
            
            }
));

}

 module.exports = initialize;