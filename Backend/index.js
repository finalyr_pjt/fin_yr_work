const express = require('express');
const app = express();

const seed = require("./seedDB");
const methodOverride = require("method-override");
const bodyParser = require("body-parser");
// const session = require("express-session");
const passport = require("passport"),
      axios = require('axios');

const router = express.Router();
const initializePassport = require("./passportConfig");
initializePassport(passport);



app.use(express.json());
app.use(bodyParser.urlencoded({extended:true}));
// app.use(
// 	session ({
// 		secret: "This backend was created by Tarun",
// 		resave : false,
// 		saveUninitialized: false
// 	})
// );

router.use(express.static(__dirname+"./public/"));
app.use(passport.initialize());
// app.use(passport.session());
       

app.use(express.static("public"));   
app.use(methodOverride('_method'));

axios.defaults.baseURL = process.env.BASE_URL

//Seeding
// seed.reset(); // when running the application first time
 seed.execute();


// For CORS error

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', '*');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});


// Routes
const pagesRouters = require("./routes/pages");
const testerRouters = require("./routes/tester");
const devRouters = require("./routes/dev"); 

 
app.use(pagesRouters); 
app.use(testerRouters); 
app.use(devRouters);  

// Server config
app.listen(process.env.PORT || 8000, process.env.IP,()=>{
	console.log("Server started");
}) 