const express = require("express"),
	  router = express.Router();
               

const passport = require("passport");
const bcrypt = require("bcrypt");
const { pool } = require("../db"); 
const mailTransporter =  require("../nodemailerConfig");
const s3Obj =  require("../s3Config");
const otpGenerator = require("otp-generator");
let otp='',expiration_time='';

// To add minutes to the current time
function AddMinutesToDate(date, minutes) {
    return new Date(date.getTime() + minutes*60000);
  }
 
router.get("/",async(req,res)=>{
    await pool.query("SELECT PROJECT_ID,TITLE,PLATFORM,TYPEOF,POINT_REWARDS,PIC_URL FROM DISP_ALL_PROJECT WHERE ENROLLMENT_ENDS_ON > NOW();",(err,result)=>{
        if(err){
            res.json("Data unavaliable");
        }
        if(result){
            
            res.json(result.rows);
              
        } 
    }) 
});

router.get("/projects/android",async(req,res)=>{
    await pool.query("SELECT PROJECT_ID,TITLE,PLATFORM,TYPEOF,POINT_REWARDS,PIC_URL,ENROLLMENT_ENDS_ON FROM DISP_ALL_PROJECT WHERE ENROLLMENT_ENDS_ON > NOW() AND PROJECT_ID IN (SELECT PROJECT_ID FROM PROJECT WHERE PLATFORM IN (0,3,4,6));",(err,result)=>{
        if(err){
            res.json("Data unavaliable");
        }
        if(result){
            res.json(result.rows);
              
        }
    }) 
});

router.get("/projects/website",async(req,res)=>{
    await pool.query("SELECT PROJECT_ID,TITLE,PLATFORM,TYPEOF,POINT_REWARDS,PIC_URL,ENROLLMENT_ENDS_ON FROM DISP_ALL_PROJECT WHERE ENROLLMENT_ENDS_ON > NOW() AND PROJECT_ID IN (SELECT PROJECT_ID FROM PROJECT WHERE PLATFORM IN (2,4,5,6));",(err,result)=>{
        if(err){
            res.json("Data unavaliable");
        }
        if(result){
            res.json(result.rows);
              
        }
    }) 
});

router.get("/projects/ios",async(req,res)=>{
    await pool.query("SELECT PROJECT_ID,TITLE,PLATFORM,TYPEOF,POINT_REWARDS,PIC_URL,ENROLLMENT_ENDS_ON FROM DISP_ALL_PROJECT WHERE ENROLLMENT_ENDS_ON > NOW() AND PROJECT_ID IN (SELECT PROJECT_ID FROM PROJECT WHERE PLATFORM IN (1,3,5,6));",(err,result)=>{
        if(err){
            res.json("Data unavaliable");
        }
        if(result){
            res.json(result.rows);
              
        }
    }) 
});

router.get("/projects/ui",async(req,res)=>{
    await pool.query("SELECT PROJECT_ID,TITLE,PLATFORM,TYPEOF,POINT_REWARDS,PIC_URL,ENROLLMENT_ENDS_ON FROM DISP_ALL_PROJECT WHERE ENROLLMENT_ENDS_ON > NOW() AND PROJECT_ID IN (SELECT PROJECT_ID FROM PROJECT WHERE TYPEOF IN (0,2));",(err,result)=>{
        if(err){
            res.json("Data unavaliable");
        }
        if(result){
            res.json(result.rows);
              
        }
    }) 
});

router.get("/projects/functionality",async(req,res)=>{
    await pool.query("SELECT PROJECT_ID,TITLE,PLATFORM,TYPEOF,POINT_REWARDS,PIC_URL,ENROLLMENT_ENDS_ON FROM DISP_ALL_PROJECT WHERE ENROLLMENT_ENDS_ON > NOW() AND PROJECT_ID IN (SELECT PROJECT_ID FROM PROJECT WHERE TYPEOF IN (1,2));",(err,result)=>{
        if(err){
            res.json("Data unavaliable");
        }
        if(result){
            res.json(result.rows);
              
        } 
    })  
}); 

              
router.get("/project/:projectid",async(req,res)=>{
    try {
        const {projectid} = req.params; 
        await pool.query("SELECT * FROM DISP_ALL_PROJECT WHERE ENROLLMENT_ENDS_ON > NOW() AND PROJECT_ID = $1;",[projectid],async(err,result)=>{
        if(err){
            res.json("Data unavaliable");
        }
        if(result){
            await pool.query("SELECT terms_id,description FROM terms_condn WHERE project_id = $1;",[projectid],async(err,termsncondn)=>{
                if(err){
                    res.json(result.rows[0]);
                }
                if(termsncondn){
                    await pool.query("SELECT CUSTOM_TESTING_PAGES_id,ss_url,description,pages_title FROM CUSTOM_TESTING_PAGES WHERE project_id = $1;",[projectid],(err,cust_test_pg)=>{
                        if(err){
                            res.json({proj_detail:result.rows[0],termsandcond:termsncondn.rows});
                        }
                        if(cust_test_pg){
                            res.json({proj_detail:result.rows[0],termsandcond:termsncondn.rows,custpgdetail:cust_test_pg.rows});
                            
                        }
                    })
                } 
            })
              
        } 
    })
         
    } catch (error) {
        res.json(error);
    }
    
});

router.post("/contactUs", (req, res) => {
    try {
        const { name, email,phoneno, message } = req.body;
        const details = {
            from: process.env.MAIL_ID,
            // to : "ts137137@gmail.com,adnanhaque36@gmail.com,itssocialme@gmail.com,tanay201705@gmail.com",
            to : "ts137137@gmail.com,tarun1103sharma@gmail.com",
            subject: "Someone wants to contact us",
            text: "name- "+name+"\nemailid - "+email+"\nphone number- "+phoneno+"\nmesssage- "+message
        }
        mailTransporter.sendMail(details,(err)=>{
            if(err)
                res.json({error:"Mail sending was Unsuccessfull"});
            else
                res.json({msg:"Someone wants to contact us"});
        })
    } catch (error) {
        console.error(err.message);   
    }
});

router.get("/images/:key",(req,res)=>{
    const key = req.params.key;

    const readStream = s3Obj.getFileStream(key);
         readStream.pipe(res)

}); 
 
router.post("/forgot-or-reset-password",async(req,res)=>{
    try {
        const {email} = req.body;
        await pool.query("SELECT count(*) FROM DISP_TESTER WHERE email = $1;",[email],async(err,result)=>{
            if(err){
                res.json("Data unavaliable");
            }  
            if(result.rows[0].count==1){
                otp = otpGenerator.generate(6, { lowerCaseAlphabets:false,upperCaseAlphabets: false, specialChars: false });
                const now = new Date();
                expiration_time = AddMinutesToDate(now,10);
                const details = {
                from: process.env.MAIL_ID,
                to : "ts137137@gmail.com,"+email,
                subject: "Your OTP is ",
                text: "Your OTP is "+otp+ " ,Please Note it will expire in 10 minutes or on"+expiration_time
                }
                mailTransporter.sendMail(details,(err)=>{
                    if(err)
                        res.json({error:"Mail sending was Unsuccessfull"});
                    else
                        res.json({email:email}); 
                })
            }
            if(result.rows[0].count==0){
                await pool.query("SELECT count(*) FROM DISP_DEVELOPER WHERE email = $1;",[email],async(err,result)=>{
                    if(err){
                        res.json("Data unavaliable");
                    }
                    if(result.rows[0].count==1){
                        otp = otpGenerator.generate(6, { lowerCaseAlphabets:false,upperCaseAlphabets: false, specialChars: false });
                        const now = new Date();
                        expiration_time = AddMinutesToDate(now,10);
                        const details = {
                            from: process.env.MAIL_ID,
                            to : "ts137137@gmail.com,"+email,
                            subject: "Your OTP is ",
                            text: "Your OTP is "+otp+ ",Please Note it will expire in 10 minutes or on"+expiration_time
                            }
                            mailTransporter.sendMail(details,(err)=>{
                                if(err)
                                    res.json({error:"Mail sending was Unsuccessfull"});
                                else
                                    res.json({email:email});
                            })
                    }
                    if(result.rows[0].count==0){
                        res.json({err:"Email is not registered"})
                    }
                })
            }
        });
    } catch (error) {
        
    }
})
router.post("/verify-otp-and-password-reset",async(req,res)=>{
    try {
        const {userotp,password,password2,email} = req.body;  
        const now = new Date();
            if(otp==userotp && expiration_time && now<=expiration_time)
            {
                await pool.query("SELECT count(*) FROM DISP_TESTER WHERE email = $1;",[email],async(err,result)=>{
                    if(err){
                        res.json("Data unavaliable");
                    }
                    if(result.rows[0].count==1){
                        let errors = []
		
                        if(!password || !password2)
                        {errors.push({message:"Please enter all the field correctly"});} 
                        if(password.length < 6)
                        {errors.push({message:"Please enter all the field correctly"});} 
                        if(password!=password2)
                        {errors.push({message:"Please enter all the field correctly"});} 
                        if(errors.length > 0){
                            res.send("Error length");
                        } 
                    
                        else{    
                            
                            const hashedpassword = await bcrypt.hash(password,10);
                            await pool.query("update tester set password = $1 where email=$2",
                            [hashedpassword,email],
                            (err,results)=>{
                                if(results){
                                    res.json({msg:"succesfully password reset"})
                                }  
                                if(err){
                                    console.log(err);
                                }
                            }); 
                        }
                    }
                    if(result.rows[0].count==0){
                        await pool.query("SELECT count(*) FROM DISP_DEVELOPER WHERE email = $1;",[email],async(err,result)=>{
                            if(err){
                                res.json("Data unavaliable");
                            }
                            if(result.rows[0].count==1){
                                let errors = []
		
                                if(!password || !password2)
                                {errors.push({message:"Please enter all the field correctly"});} 
                                if(password.length < 6)
                                {errors.push({message:"Please enter all the field correctly"});} 
                                if(password!=password2)
                                {errors.push({message:"Please enter all the field correctly"});} 
                                if(errors.length > 0){
                                    res.send("Error length");
                                }
                            
                                else{    
                                    
                                    const hashedpassword = await bcrypt.hash(password,10);
                                    await pool.query("update developer set password = $1 where email=$2",
                                    [hashedpassword,email],
                                    (err,results)=>{
                                        if(results){
                                            res.json({msg:"succesfully password reset"})
                                        }  
                                        if(err){
                                            console.log(err);
                                        }
                                    }); 
                                }
                            }
                            if(result.rows[0].count==0){
                                res.json({err:"Email is not registered"})
                            }
                        })
                    }
                })
               
            }    
            else
                res.json({err:"User not verified"})
        }
    catch(err){

    }
})

router.get("/rewards/fashion",async(req,res)=>{
    await pool.query("SELECT * FROM REWARDS WHERE REWARDTYPE=0;",(err,result)=>{
        if(err){
            res.json("Data unavaliable");
        }
        if(result){
            res.json(result.rows);
              
        }
    }) 
});

router.get("/rewards/online-retail",async(req,res)=>{
    await pool.query("SELECT * FROM REWARDS WHERE REWARDTYPE=1;",(err,result)=>{
        if(err){
            res.json("Data unavaliable");
        }
        if(result){
            res.json(result.rows);
              
        }
    }) 
});

router.get("/rewards/food",async(req,res)=>{
    await pool.query("SELECT * FROM REWARDS WHERE REWARDTYPE=2;",(err,result)=>{
        if(err){
            res.json("Data unavaliable");
        }
        if(result){
            res.json(result.rows);
              
        }
    }) 
});

router.get("/rewards/gift-coupon",async(req,res)=>{
    await pool.query("SELECT * FROM REWARDS WHERE REWARDTYPE=3;",(err,result)=>{
        if(err){
            res.json("Data unavaliable");
        }
        if(result){
            res.json(result.rows);
              
        }
    }) 
});

module.exports = router; 