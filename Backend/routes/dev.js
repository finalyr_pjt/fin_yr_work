const express = require("express"),
    router = express.Router(),
    jwt = require('jsonwebtoken'),
    fs = require('fs'),
    axios = require('axios');


const passport = require("passport");
const bcrypt = require("bcrypt");
const { pool } = require("../db");
const mailTransporter =  require("../nodemailerConfig");
const middleware = require("../middleware");
 

router.get("/dev/dashboard", passport.authenticate('ifdevloperAuthenticated', { session: false }), async (req, res) => {
    try {
        const user = req.user;
        await pool.query("SELECT D.DEVELOPER_ID,D.NAME,D.EMAIL,D.developer_type,D.profile_created_on,D.pic_url,D.about_us,P.PROJECT_ID,P.TITLE,P.TESTING_SUBMISSION_BEFORE,P.STATUS FROM DISP_DEVELOPER D LEFT JOIN DISP_DEV_PROJECT P ON D.DEVELOPER_ID = P.DEVELOPER_ID WHERE D.developer_id = $1;", [user.developer_id], (err, result) => {
            if (err) {
                res.json("Data unavaliable");
            }  
            if (result) { 
                res.json(result.rows);   
 
            }  
        }) 
    } 
    catch (err) {
        res.send(err);
    }
});
 
router.get("/dev/viewprojectresponses/:projectid", passport.authenticate('ifdevloperAuthenticated', { session: false }), async (req, res) => {
    try {
        const user = req.user;
        const { projectid } = req.params;
        await pool.query("SELECT ENROLLMENT_ID,T.NAME,E.ENROLLED_ON,E.SUBMITTED_ON,P.TITLE,P.POINT_REWARDS,case when P.TESTING_PAGE_TYPE = 0 then (select count(*) from issues_report where enrollment_id = e.enrollment_id)when P.TESTING_PAGE_TYPE =1 then (select count(*) from CUSTOM_PAGES_ISSUE_REPORT where tester_id = t.tester_id and enrollment_id = e.enrollment_id)end as totalissuereported FROM ENROLLMENT E JOIN DISP_TESTER T ON E.TESTER_ID=T.TESTER_ID JOIN PROJECT P ON P.PROJECT_ID=E.PROJECT_ID WHERE E.STATUS=1 AND P.PROJECT_ID = $1 AND P.DEVELOPER_ID=$2",
        [projectid, user.developer_id], async (err, result) => {
            if (err) {
                res.json("Updation Unsuccessfull");
            } 
            if (result) {
                res.json(result.rows); 
            } 
        });
    }
    catch (err) {
        res.json("Error");
    }
});

router.get("/dev/responses/:projectid/:enrollmentid", passport.authenticate('ifdevloperAuthenticated', { session: false }), async (req, res) => {
    try {
        const user = req.user;
        const { projectid,enrollmentid } = req.params;
        await pool.query("SELECT T.email,T.TESTER_ID,T.NAME,T.ACTIVE,E.ENROLLED_ON,E.SUBMITTED_ON,P.TITLE,P.POINT_REWARDS,P.TESTING_PAGE_TYPE FROM ENROLLMENT E JOIN TESTER T ON E.TESTER_ID=T.TESTER_ID JOIN PROJECT P ON P.PROJECT_ID=E.PROJECT_ID WHERE E.STATUS=1 AND P.PROJECT_ID = $1 AND P.DEVELOPER_ID=$2 AND E.ENROLLMENT_ID=$3",
        [projectid, user.developer_id,enrollmentid], async (err, result) => {
            if (result.rows.length) { 
                const testing_type = result.rows[0].testing_page_type; 
                const tester_id = result.rows[0].tester_id;
                if(testing_type===0){
                    await pool.query("SELECT * FROM DISP_ALL_ISSUES_REPORT WHERE ENROLLMENT_ID =$1",
                    [enrollmentid], async (err, issues) => {
                        if (err) {
                            res.json("Updation Unsuccessfull");
                        }
                        if (issues) {
                            res.json({projectDetail:result.rows[0],issues:issues.rows}); 
                        }
                    });
                }
                else   
                    if(testing_type===1){
                    await pool.query("SELECT ISSUE_TYPE,PAGES_TITLE,IMG_URL,C.DESCRIPTION FROM DISP_ALL_CUST_PG_ISS_REP C JOIN CUSTOM_TESTING_PAGES T ON C.CUSTOM_TESTING_PAGES_ID=T.CUSTOM_TESTING_PAGES_ID WHERE TESTER_ID =$1 AND ENROLLMENT_ID = $2;",
                    [tester_id,enrollmentid], async (err, issues) => {
                        if (err) {
                            res.json("Updation Unsuccessfull");
                        }
                        if (issues) {
                            res.json({projectDetail:result.rows[0],issues:issues.rows}); 
                        }
                    });
                    }
                
            }
            else
            {
                res.json("Updation Unsuccessfull");
            }
        });
    }
    catch (err) {
        res.json("Error");
    }
});

router.get("/dev/viewprojectgranted/:projectid", passport.authenticate('ifdevloperAuthenticated', { session: false }), async (req, res) => {
    try {
        const user = req.user;
        const { projectid } = req.params;
        await pool.query("SELECT ENROLLMENT_ID,T.NAME,E.ENROLLED_ON,E.SUBMITTED_ON,E.REWARDED_ON,E.POINTS_RECIEVED,P.TITLE,P.POINT_REWARDS,case when P.TESTING_PAGE_TYPE = 0 then (select count(*) from issues_report where enrollment_id = e.enrollment_id)when P.TESTING_PAGE_TYPE =1 then (select count(*) from CUSTOM_PAGES_ISSUE_REPORT where tester_id = t.tester_id and enrollment_id = e.enrollment_id)end as totalissuereported FROM ENROLLMENT E JOIN DISP_TESTER T ON E.TESTER_ID=T.TESTER_ID JOIN PROJECT P ON P.PROJECT_ID=E.PROJECT_ID WHERE E.STATUS=2 AND P.PROJECT_ID = $1 AND P.DEVELOPER_ID=$2",
        [projectid, user.developer_id], async (err, result) => {
            if (err) {
                res.json("Updation Unsuccessfull");
            } 
            if (result) {
                res.json(result.rows); 
            } 
        }); 
    } 
    catch (err) {
        res.json("Error");
    }
});

router.get("/dev/granted/:projectid/:enrollmentid", passport.authenticate('ifdevloperAuthenticated', { session: false }), async (req, res) => {
    try {
        const user = req.user;
        const { projectid,enrollmentid } = req.params;
        await pool.query("SELECT T.email,T.TESTER_ID,T.NAME,T.ACTIVE,E.ENROLLED_ON,E.SUBMITTED_ON,P.TITLE,E.REWARDED_ON,E.POINTS_RECIEVED,P.POINT_REWARDS,P.TESTING_PAGE_TYPE FROM ENROLLMENT E JOIN TESTER T ON E.TESTER_ID=T.TESTER_ID JOIN PROJECT P ON P.PROJECT_ID=E.PROJECT_ID WHERE E.STATUS=2 AND P.PROJECT_ID = $1 AND P.DEVELOPER_ID=$2 AND E.ENROLLMENT_ID=$3",
        [projectid, user.developer_id,enrollmentid], async (err, result) => {
            if (result.rows.length) { 
                const testing_type = result.rows[0].testing_page_type; 
                const tester_id = result.rows[0].tester_id;
                if(testing_type===0){
                    await pool.query("SELECT * FROM DISP_ALL_ISSUES_REPORT WHERE ENROLLMENT_ID =$1",
                    [enrollmentid], async (err, issues) => {
                        if (err) {
                            res.json("Updation Unsuccessfull");
                        }
                        if (issues) {
                            res.json({projectDetail:result.rows[0],issues:issues.rows}); 
                        }
                    });
                }
                else  
                    if(testing_type===1){
                    await pool.query("SELECT ISSUE_TYPE,PAGES_TITLE,IMG_URL,C.DESCRIPTION FROM DISP_ALL_CUST_PG_ISS_REP C JOIN CUSTOM_TESTING_PAGES T ON C.CUSTOM_TESTING_PAGES_ID=T.CUSTOM_TESTING_PAGES_ID WHERE TESTER_ID =$1 AND ENROLLMENT_ID = $2;",
                    [tester_id,enrollmentid], async (err, issues) => {
                        if (err) {
                            res.json("Updation Unsuccessfull");
                        }
                        if (issues) {
                            res.json({projectDetail:result.rows[0],issues:issues.rows}); 
                        }
                    });
                    }
                
            }
            else
            {
                res.json("Updation Unsuccessfull");
            }
        });
    }
    catch (err) {
        res.json("Error");
    }
});

router.post("/dev/grantpoints", passport.authenticate('ifdevloperAuthenticated', { session: false }), async (req, res) => {
    try {
        const { testerid,enrollmentid,points,emailid } = req.body;
        await pool.query("UPDATE ENROLLMENT SET STATUS = 2,REWARDED_ON=CURRENT_DATE,POINTS_RECIEVED=($1)::INT WHERE TESTER_ID=$2 AND ENROLLMENT_ID=$3",
        [points,testerid,enrollmentid], async (err, result) => {
            if(err){
                res.json({error:"Updation Unsuccessfull"});
            }
            if(result){
                await pool.query("UPDATE TESTER SET REWARD_POINTS = REWARD_POINTS+($1)::INT WHERE TESTER_ID=$2",
                [points,testerid], async (err, result) => {
                    if(err){
                        res.json({error:"Updation Unsuccessfull"});
                    }
                    if(result){
                        const details = {
                            from: process.env.MAIL_ID,
                            to : emailid,
                            subject: "Congrats! Your submission has granted you reward points",
                            text: "Dear Tester, Your recent issue reporting is been appreciated by the developers of the project which has lead you to recieve "+points+" points.Keep up the good work."
                        }
                        mailTransporter.sendMail(details,(err)=>{
                            if(err)
                                res.json({error:"Mail sending was Unsuccessfull"});
                            else
                                res.json({msg:"Successfull Updation"});
                        })
                    }
                });
            }
        });
    }
    catch (err) {
        res.json("Error");
    }
}); 
   
   
router.post("/dev/downvote", passport.authenticate('ifdevloperAuthenticated', { session: false }), async (req, res) => {
    try {
        const { testerid,enrollmentid,emailid} = req.body;
        await pool.query("UPDATE ENROLLMENT SET STATUS = 2,REWARDED_ON=CURRENT_DATE,POINTS_RECIEVED=0 WHERE TESTER_ID=$1 AND ENROLLMENT_ID=$2",
        [testerid,enrollmentid], async (err, result) => {
            if(err){
                res.json({error:"Updation Unsuccessfull"});
            }
            if(result){
                await pool.query("UPDATE TESTER SET BLOCK_COUNT = BLOCK_COUNT+1 WHERE TESTER_ID=$1",
                [testerid], async (err, result) => {
                    if(err){
                        res.json({error:"Updation Unsuccessfull"});
                    } 
                    if(result){
                        // SET MAIL TO THE TESTER ABOUT ACCOUNT BLOCKING.
                        await pool.query("SELECT BLOCK_COUNT FROM TESTER WHERE TESTER_ID=$1",
                        [testerid], async (err, blockcount) => {
                            if(err){
                                res.json({error:"Updation Unsuccessfull"});
                            }
                            if(blockcount){
                                // SET MAIL TO THE TESTER ABOUT ACCOUNT BLOCKING.
                                const blocks = (blockcount.rows[0].block_count);
                                let msg;
                                if(blocks>=3){
                                    await pool.query("UPDATE TESTER SET ACTIVE=0 WHERE TESTER_ID=$1",
                                    [testerid], async (err, result) => {
                                        if(err){
                                            res.json({error:"Updation Unsuccessfull"});
                                        }  
                                        if(result){
                                            // SET MAIL TO THE TESTER ABOUT ACCOUNT BLOCKING.
                                            const details = {
                                                from: process.env.MAIL_ID,
                                                to : emailid,
                                                subject: "Your submission was downvoted",
                                                text: "Dear Tester, We are sorry to say the your recent issue reporting was disliked by the developers of the project.Your block counter is "+blocks+" ,please make sure that you try harder in the next project the you enroll."
                                            }
                                            mailTransporter.sendMail(details,(err)=>{
                                                if(err)
                                                    msg = "Mail sending was Unsuccessfull";
                                                
                                            })  
                                        }
                                    });
                                    res.json({msg:"Successfull Updation"});
                                }
                                else{
                                    res.json({error:"blockcount is 3 "});
                                }
                                
                            }
                        });
                        
                    }
                });
            }
        });
    }  
    catch (err) {
        res.json("Error");
    }
});

router.post("/dev/createnewprojectdemo",middleware.upload.array('images'), async (req, res) => { //,middleware.upload.fields('images')
    try {
        const filesKeys = req.files;
        console.log(filesKeys)
        // res.json({msg:filesKeys}); 
        
        
    } 
    catch (err) {
        res.send(err);
    }
});

router.post("/dev/createnewproject",[middleware.upload.array('images'), passport.authenticate('ifdevloperAuthenticated', { session: false })], async (req, res) => { //
    try {
        
        const user = req.user;
        const { title, tsb, eeo, iss_type, points, age_upper, age_lower, test_type, gen, about, plaform, type } = req.body;
        const filesKeys = req.files;
        await pool.query("INSERT INTO PROJECT (TITLE,TESTING_SUBMISSION_BEFORE,ENROLLMENT_ENDS_ON,ISSUE_TYPE,POINT_REWARDS,AGE_UPPER_LIMIT,AGE_LOWER_LIMIT,TESTING_PAGE_TYPE,GENDER,ABOUT_TASK,PLATFORM,TYPEOF,DEVELOPER_ID) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13) RETURNING PROJECT_ID;", [title, tsb, eeo, iss_type, points, age_upper, age_lower, test_type, gen, about, plaform, type, user.developer_id], async (err, result) => {
            if (err) {
                res.json(err);
            }     
            if (result) {  
                let flag = 1; 
                
                const project_id = (result.rows[0].project_id);
                if (test_type == 1) {
                    let j=0;
                    for (let i = 0; i < req.body.lengthCust; i++) {
                        const  descp  = req.body.lengthCust ==1?req.body.descp:req.body.descp[i];
                        const  pgtitle = req.body.lengthCust ==1?req.body.pgtitle:req.body.pgtitle[i];
                        
                        if(req.body.imageIndex)
                        {
                            if(req.body.imageIndex.includes(i.toString()))
                            {   const url = req.files[j].key; 
                                await pool.query("CALL ADD_CUSTOM_TESTING_PAGES($1,$2,$3,$4);", [project_id, url, descp, pgtitle], (err, cust_pg) => {
                                    if (err) {
                                        flag = 0
                                    } 
                                    if (cust_pg) {
                                        
                                    }
                                })
                                j++;
                            }
                            else{
                                await pool.query("CALL ADD_CUSTOM_TESTING_PAGES($1,$2,$3,$4);", [project_id, '', descp, pgtitle], (err, cust_pg) => {
                                    if (err) {
                                        flag = 0
                                    } 
                                    if (cust_pg) {
                                        
                                    }
                                })
                            }
                        }
                        else{
                            await pool.query("CALL ADD_CUSTOM_TESTING_PAGES($1,$2,$3,$4);", [project_id, '', descp, pgtitle], (err, cust_pg) => {
                                if (err) {
                                    flag = 0
                                } 
                                if (cust_pg) {
                                    
                                }
                            })
                        }
                    } 

                }
                for (let i = 0; i < req.body.lengthTerms; i++) {
                    const  descp  = req.body.lengthTerms ==1?req.body.termdescp:req.body.termdescp[i];
                    if(descp){
                        await pool.query("CALL ADD_TERMS_CONDN($1,$2);", [project_id, descp], (err, terms) => {
                            if (err) {
                                flag = 0    
                            }
                            if (terms) { 
                            } 
                            
                        })
                    }
                }

                flag == 1 ? res.json({ msg: "Project is created" }) : res.json({ error: "some error" });
            }
        })
    }
    catch (err) {
        res.send(err);
        // res.json({error:"Some error try to upload image again"});
    } 
});
router.get("/dev/updateproject/:projectid", passport.authenticate('ifdevloperAuthenticated', { session: false }), async (req, res) => {
    try {
        const user = req.user;
        const { projectid } = req.params;
        await pool.query("SELECT * FROM PROJECT WHERE PROJECT_ID = $1 AND DEVELOPER_ID = $2", [projectid, user.developer_id], async (err, result) => {
            if (err) {
                res.json("Updation Unsuccessfull");
            }
            if (result) {
                await pool.query("SELECT terms_id,description FROM terms_condn WHERE project_id = $1;", [projectid], async (err, termsncondn) => {
                    if (err) {
                        res.json(result.rows[0]);
                    }
                    if (termsncondn) {
                        await pool.query("SELECT CUSTOM_TESTING_PAGES_id,ss_url,description,pages_title FROM CUSTOM_TESTING_PAGES WHERE project_id = $1;", [projectid], (err, cust_test_pg) => {
                            if (err) {
                                res.json({ proj_detail: result.rows[0], termsandcond: termsncondn.rows });
                            }
                            if (cust_test_pg) {
                                res.json({ proj_detail: result.rows[0], termsandcond: termsncondn.rows, custpgdetail: cust_test_pg.rows });

                            }
                        })
                    }
                })

            }
        })
    }
    catch (err) {
        res.send(err);
    }
});

router.post("/dev/updateproject/:projectid",[middleware.upload.array('images'), passport.authenticate('ifdevloperAuthenticated', { session: false })], async (req, res) => {
    try {
        const user = req.user;
        const { projectid } = req.params;
        const { title, tsb, eeo, iss_type, points, age_upper, age_lower, test_type, gen, about, plaform, type } = req.body;
        
        await pool.query("CALL UPDATE_PROJECT($1,$2,$3::DATE,$4::DATE,$5::INTEGER,$6::INTEGER,$7::INTEGER,$8::INTEGER,$9::INTEGER,$10::INTEGER,$11,$12::INTEGER,$13::INTEGER,$14)", [projectid, title, tsb, eeo, iss_type, points, age_upper, age_lower, test_type, gen, about, plaform, type, user.developer_id], async (err, result) => {
            if (err) {

                res.json("Updation Unsuccessfull"); 
            }
            if (result) {  
                let flag = 1;

                if (test_type == 1) {
                    let j=0;
                    for (let i = 0; i < req.body.lengthCust; i++) {
                        const  descp  = req.body.lengthCust ==1?req.body.descp:req.body.descp[i];
                        const  pgtitle = req.body.lengthCust ==1?req.body.pgtitle:req.body.pgtitle[i];
                        const custid = req.body.lengthCust ==1?req.body.custid:req.body.custid[i];
                        
                        if(req.body.imageIndex)
                        {
                            if(req.body.imageIndex.includes(i.toString()))
                            {   const url = req.files[j].key;
                                if (custid ) {
                                    await pool.query("UPDATE CUSTOM_TESTING_PAGES SET ss_url=$1,description=$2,pages_title=$3 WHERE CUSTOM_TESTING_PAGES_id=$4 AND project_id=$5;", [url, descp, pgtitle, custid, projectid], (err, cust_pg) => {
                                        if (err) {
                                            flag = 0
                                        }
                                        if (cust_pg) { 
        
                                            
                                        }
                                    })
                                }
                                
                                else 
                                    {
                                    await pool.query("CALL ADD_CUSTOM_TESTING_PAGES($1,$2,$3,$4);", [projectid, url, descp, pgtitle], (err, cust_pg) => {
                                        if (err) {
                                            flag = 0
                                        }
                                        if (cust_pg) {
                                            
                                        }
                                    })
                                }
                                j++;
                            }
                            else{

                                if (custid ) {
                                    await pool.query("UPDATE CUSTOM_TESTING_PAGES SET description=$1,pages_title=$2 WHERE CUSTOM_TESTING_PAGES_id=$3 AND project_id=$4;", [descp, pgtitle, custid, projectid], (err, cust_pg) => {
                                        if (err) {
                                            flag = 0
                                        }
                                        if (cust_pg) { 
        
                                        }
                                    })
                                }
                                
                                else 
                                    {
                                    await pool.query("CALL ADD_CUSTOM_TESTING_PAGES($1,'',$2,$3);", [projectid, descp, pgtitle], (err, cust_pg) => {
                                        if (err) {
                                            flag = 0
                                        }
                                        if (cust_pg) {
                                        }
                                    })
                                }
                            }
                        }
                        else{

                            if (custid ) {
                                await pool.query("UPDATE CUSTOM_TESTING_PAGES SET description=$1,pages_title=$2 WHERE CUSTOM_TESTING_PAGES_id=$3 AND project_id=$4;", [descp, pgtitle, custid, projectid], (err, cust_pg) => {
                                    if (err) {
                                        flag = 0
                                    }
                                    if (cust_pg) { 
    
                                    }
                                })
                            }
                            
                            else 
                                {
                                await pool.query("CALL ADD_CUSTOM_TESTING_PAGES($1,'',$2,$3);", [projectid, descp, pgtitle], (err, cust_pg) => {
                                    if (err) {
                                        flag = 0
                                    }
                                    if (cust_pg) {
                                    }
                                })
                            }
                        }
                    }
  
                }
                for (let i = 0; i < req.body.lengthTerms; i++) {
                    const  descp  = req.body.lengthTerms ==1?req.body.termdescp:req.body.termdescp[i];
                    const  termid  = req.body.lengthTerms ==1?req.body.termid:req.body.termid[i];
                    if(descp)
                    {
                        if (termid) {
                            await pool.query("UPDATE terms_condn SET description=$1 WHERE terms_id=$2 AND project_id=$3;", [descp, termid, projectid], (err, terms) => {
                                if (err) {
                                    res.json({ error: "some error" });
                                }
                                if (terms) {
                                    flag = 0 
                                }
                            })
                        }
                        else {
                            await pool.query("CALL ADD_TERMS_CONDN($1,$2);", [projectid, descp], (err, terms) => {
                                if (err) {
                                    res.json({ error: "some error" });
                                }
                                if (terms) {
                                    flag = 0
                                }
                            })
                        }
                    }
                }

                flag == 1 ? res.json({ msg: "Project is Updated" }) : res.json({ error: "some error" });

            }
        })
    }
    catch (err) {
        res.send(err);
    }
});
 
router.delete("/dev/custpg/delete/:custid", passport.authenticate('ifdevloperAuthenticated', { session: false }),async(req,res)=>{ 
	try {
		const {custid} = (req.params);
		await pool.query("CALL DEL_CUSTOM_TESTING_PAGES($1);", [custid], (err, updatestatus) => {
            if (err) {
                res.json({ error: "Data unavaliable" ,err:err});
            }
            if (updatestatus) { 
                res.json({ msg: "Custom page is deleted" });
            } 
   
    
        })  
		  
	} catch (err) { 
		console.error(err.message);
	}
}); 

router.delete("/dev/terms/delete/:termid", passport.authenticate('ifdevloperAuthenticated', { session: false }),async(req,res)=>{ 
	try {
		const {termid} = (req.params);
		await pool.query("DELETE FROM TERMS_CONDN WHERE terms_id=$1;", [termid], (err, updatestatus) => {
            if (err) {
                res.json({ error: "Data unavaliable" ,err:err});
            }
            if (updatestatus) { 
                res.json({ msg: "Terms is deleted" });
            } 
   
    
        })  
		 
	} catch (err) { 
		console.error(err.message);
	}
}); 


router.get("/dev/active_project/:projectid", passport.authenticate('ifdevloperAuthenticated', { session: false }), async (req, res) => {
    try {
        const user = req.user;
        const { projectid } = req.params;

        await pool.query("UPDATE PROJECT SET STATUS = 1 WHERE developer_id = $1 and project_id = $2;", [user.developer_id, projectid], (err, updatestatus) => {
            if (err) {
                res.json({ error: "Data unavaliable" });
            }
            if (updatestatus) {
                res.json({ msg: "Project is now deactived" });
            }


        })
    }
    catch (err) {
        res.send(err);
    }
});

router.get("/dev/deactive_project/:projectid", passport.authenticate('ifdevloperAuthenticated', { session: false }), async (req, res) => {
    try {
        const user = req.user;
        const { projectid } = req.params;

        await pool.query("UPDATE PROJECT SET STATUS = 0 WHERE developer_id = $1 and project_id = $2;", [user.developer_id, projectid], (err, result) => {
            if (err) {
                res.json({ error: "Data unavaliable" });
            }
            if (result) {
                res.json({ msg: "Project is now deactived" });
            }

        })
    }
    catch (err) {
        res.send(err); 
    }
});

router.get("/dev/project/:projectid", passport.authenticate('ifdevloperAuthenticated', { session: false }), async (req, res) => {
    try {
        const user = req.user;
        const { projectid } = req.params;
        await pool.query("SELECT P.*,D.NAME,D.ABOUT_US FROM DISP_DEV_PROJECT P JOIN DISP_DEVELOPER D ON P.DEVELOPER_ID=D.DEVELOPER_ID WHERE P.developer_id = $1 and project_id = $2;", [user.developer_id, projectid], async (err, result) => {
            if (err) {
                res.json("Data unavaliable");
            }
            if (result) {

                await pool.query("SELECT terms_id,description FROM terms_condn WHERE project_id = $1;", [projectid], async (err, termsncondn) => {
                    if (err) {
                        res.json(result.rows[0]);
                    }
                    if (termsncondn) {
                        await pool.query("SELECT CUSTOM_TESTING_PAGES_id,ss_url,description,pages_title FROM CUSTOM_TESTING_PAGES WHERE project_id = $1;", [projectid], (err, cust_test_pg) => {
                            if (err) {
                                res.json({ proj_detail: result.rows[0], termsandcond: termsncondn.rows });
                            }
                            if (cust_test_pg) {
                                res.json({ proj_detail: result.rows[0], termsandcond: termsncondn.rows, custpgdetail: cust_test_pg.rows });

                            }
                        })
                    }
                })

            }
        })
    }
    catch (err) {
        res.send(err);
    }
});

router.get("/dev/myprojects", passport.authenticate('ifdevloperAuthenticated', { session: false }), async (req, res) => {
    try {
        const user = req.user;
        await pool.query("SELECT P.PROJECT_ID,TITLE,TESTING_SUBMISSION_BEFORE,CREATED_ON,ENROLLMENT_ENDS_ON,ISSUE_TYPE,AGE_LOWER_LIMIT,AGE_UPPER_LIMIT,TESTING_PAGE_TYPE,GENDER,ABOUT_TASK,P.STATUS, PLATFORM,TYPEOF,POINT_REWARDS,DEVELOPER_ID ,COUNT(CASE E.STATUS WHEN 1 THEN 1 ELSE NULL END) AS INREVIEWCOUNT FROM DISP_DEV_PROJECT P LEFT JOIN ENROLLMENT E ON P.PROJECT_ID=E.PROJECT_ID WHERE P.DEVELOPER_ID=$1 GROUP BY (P.PROJECT_ID,TITLE,TESTING_SUBMISSION_BEFORE,CREATED_ON,ENROLLMENT_ENDS_ON,ISSUE_TYPE,AGE_LOWER_LIMIT,AGE_UPPER_LIMIT,TESTING_PAGE_TYPE,GENDER,ABOUT_TASK,P.STATUS, PLATFORM,TYPEOF,POINT_REWARDS,DEVELOPER_ID ) ORDER BY P.PROJECT_ID;",
            [user.developer_id], async (err, result) => {
                if (err) {
                    res.json("Data unavaliable");
                }
                if (result) {

                    await pool.query("SELECT COUNT(*) FROM PROJECT WHERE STATUS = 0 AND DEVELOPER_ID=$1",[user.developer_id], async (err, inactive) => {
                        if (err) {
                            res.json("Data unavaliable");
                        }
                        if (inactive) {
                            await pool.query("SELECT COUNT(*) FROM PROJECT WHERE STATUS = 1 AND DEVELOPER_ID=$1",[user.developer_id], async (err, active) => {
                                if (err) {
                                    res.json("Data unavaliable");
                                }
                                if (active) {
                                    res.json({ active: active.rows[0].count, inactive: inactive.rows[0].count, projects: result.rows });
                                }
                            })
                        }
                    })

                }
            })
    }
    catch (err) {
        res.send(err);
    }
});

router.get("/dev/deactivate", passport.authenticate('ifdevloperAuthenticated', { session: false }), async(req, res) => {
    const user = req.user;
    await pool.query("UPDATE DEVELOPER SET ACTIVE=0 WHERE DEVELOPER_ID=$1",[user.developer_id],(err, result) => {
        if (err) {
            res.json("Data unavaliable");
        }
        if (result) {
            res.json({msg:"Your Profile is deactivated."});
        }
    })
});

router.post("/dev/login", (req, res) => {
    passport.authenticate('developer', { session: false }, (err, user, info) => {
        if (err || !user) {
            return res.status(400).json({
                message: 'Something is not right',
                user: user
            });
        }
        req.login(user, { session: false }, (err) => {
            if (err) {
                res.send(err); 
            }
            const token = jwt.sign(user, fs.readFileSync('private.pem', 'utf-8')/*, { expiresIn: 60 * 60 * 12 }*/);
            return res.json({ token });
        }); 
    })(req, res);
});

router.get("/dev/logout", passport.authenticate('ifdevloperAuthenticated', { session: false }), (req, res) => {
    const token = '';
    return res.json({ token });
});



router.post("/dev/createnew",middleware.upload.single('profilepic'), async (req, res) => {
    try {

        const { devname, email, password, password2, type, aboutus } = req.body;
        const url = (req.file)?req.file.key:'';
        let errors = []

        if (!devname || !email || !password || !password2) { errors.push({ message: "Please enter all the field correctly" }); }
        if (password.length < 6) { errors.push({ message: "Please enter all the field correctly" }); }
        if (password != password2) { errors.push({ message: "Please enter all the field correctly" }); }
        if (errors.length > 0) {
            res.send("Error length");
        }  
           
        else {    

            const hashedpassword = await bcrypt.hash(password, 10);

            await pool.query("insert into developer (name,email,password,developer_type,pic_url,about_us) values ($1,$2,$3,$4,$5,$6)",
                [devname, email, hashedpassword, type,url,aboutus],
                (err, results) => {
                    if (results) {
                        axios.post("/dev/login",{email:email,password:password})
                        .then((resp)=>{
                            res.json(resp.data);  
                        },(err)=>{
                            res.json({error:err})
                        })
                    }
                    if (err) {
                        console.log(err);
                    }
                });
        }

    } catch (err) {
        console.error(err.message);
    }
});

router.post("/dev/edit/:developerid",[middleware.upload.single('profilepic'),passport.authenticate('ifdevloperAuthenticated', { session: false })],async(req,res)=>{  //
	try {
		
        const {developerid} = req.params; 
		const {devname,email,type,aboutus} = req.body;

            const url = (req.file)?req.file.key:'';
		let errors = []
		
        if(!devname || !email )
        {errors.push({message:"Please enter all the field correctly"});}
        if(errors.length > 0){
            res.send("Error length");
        }
     
        else{     
            if(url)
            {
                await pool.query("update developer set name=$1,email=$2,developer_type=$3,about_us=$4,pic_url=$5 WHERE developer_id=$6;",
                [devname,email,type,aboutus,url,developerid],
                (err,results)=>{
                    if(results){
                        res.json({msg:"Succefully got updated"});
                    } 
                    if(err){
                        console.log(err);
                    }
                });
            }
            else{
                await pool.query("update developer set name=$1,email=$2,developer_type=$3,about_us=$4 WHERE developer_id=$5;",
                [devname,email,type,aboutus,developerid],
                (err,results)=>{
                    if(results){
                        res.json({msg:"Succefully got updated"});
                    } 
                    if(err){
                        console.log(err);
                    }
                });
            }
        }

	} catch (err) {
		console.error(err.message);
	}
});

module.exports = router;