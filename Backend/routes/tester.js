const express = require("express"),
	  router = express.Router(),
      jwt = require('jsonwebtoken'),
      fs = require('fs'),
      axios = require('axios');
               

const passport = require("passport");
const bcrypt = require("bcrypt");
const { pool } = require("../db");
const middleware = require("../middleware");
const mailTransporter =  require("../nodemailerConfig");
const otpGenerator = require("otp-generator");
  
router.get("/tester/dashboard",passport.authenticate('iftesterAuthenticated', {session: false}),async(req,res)=>{
    try {
        const user = req.user; 
        await pool.query("SELECT tester_id,NAME,EMAIL,GENDER,date_of_birth,profile_pic,profile_creation_time,reward_points,block_count FROM DISP_TESTER WHERE TESTER_ID = $1;",[user.tester_id],(err,result)=>{
        if(err){
            res.json("Data unavaliable");
        }
        if(result){
            
            
            res.json(result.rows[0]);
        } 
    })
    }
    catch(err){
        res.send(err);
    }
});

router.get("/tester/submit_response/:enrollid",passport.authenticate('iftesterAuthenticated', {session: false}),async(req,res)=>{
    try {
        const {enrollid} = req.params;
        const user = req.user;
        await pool.query("SELECT DISTINCT testing_submission_before,TITLE,CASE WHEN PLATFORM = 0 THEN 'Android' WHEN PLATFORM = 1 THEN 'iOS'  WHEN PLATFORM = 2 THEN 'Website' WHEN PLATFORM = 3 THEN 'Android and iOS' WHEN PLATFORM = 4 THEN 'Android and Website' WHEN PLATFORM = 5 THEN 'iOS and Website' WHEN PLATFORM = 6 THEN 'Android, iOS and Website' END PLATFORM, CASE  WHEN TYPEOF = 0 THEN 'UI' WHEN TYPEOF = 1 THEN 'Functionality'  WHEN TYPEOF = 2 THEN 'UI and Functionality' END TYPEOF,POINT_REWARDS,ENROLLED_ON FROM PROJECT JOIN ENROLLMENT ON PROJECT.PROJECT_ID = ENROLLMENT.PROJECT_ID WHERE TESTING_SUBMISSION_BEFORE > NOW() AND ENROLLMENT.ENROLLMENT_ID = $1 AND ENROLLMENT.TESTER_ID = $2",[enrollid,user.tester_id],async(err,result)=>{
            if(err){
                res.json("Data unavaliable");
                //,
            }
            if(result.rows){
                await pool.query("SELECT DISTINCT CUSTOM_TESTING_PAGES_id,PAGES_TITLE,ss_url FROM PROJECT JOIN CUSTOM_TESTING_PAGES ON PROJECT.PROJECT_ID = CUSTOM_TESTING_PAGES.PROJECT_ID JOIN ENROLLMENT ON CUSTOM_TESTING_PAGES.PROJECT_ID = ENROLLMENT.PROJECT_ID WHERE ENROLLMENT.ENROLLMENT_ID = $1 AND ENROLLMENT.TESTER_ID = $2",[enrollid,user.tester_id],(err,page_titles)=>{
                    if(err){
                        console.log("Page titles unavaliable");
                        res.json(result.rows[0]); 
                        // PAGES_TITLE,
                    }
                    if(page_titles.rows){
                        res.json({data :result.rows[0],pageTitles:page_titles.rows});
                        
                    }
                })
                
            }
        })
    }
    catch(err){
        res.json(err);
    }
});



router.post("/tester/submit_response/:enrollid",[middleware.upload.array('images'),passport.authenticate('iftesterAuthenticated', {session: false})],async(req,res)=>{
    
      
    try {
        const user = req.user;
        // let issues = req.body.data;
        const {enrollid} = req.params;
        let flag = 1;    
            await pool.query("SELECT TESTING_PAGE_TYPE FROM PROJECT WHERE TESTING_SUBMISSION_BEFORE > NOW() AND PROJECT_ID = (SELECT PROJECT_ID FROM ENROLLMENT WHERE TESTER_ID = $1 AND ENROLLMENT_ID = $2 );",[user.tester_id,enrollid],async(err,pg_type)=>{
                if(err){
                    flag =0;
                }
                if(pg_type.rows[0].testing_page_type == 0){
                    let j=0;
                    for(let i=0;i<req.body.lengthIssue;i++) {
                        const  descp  = req.body.lengthIssue ==1?req.body.descp:req.body.descp[i];
                        const issue_type = req.body.lengthIssue ==1?req.body.issue_type:req.body.issue_type[i];
                        let url='';
                        if(req.body.imageIndex && req.body.imageIndex.includes(i.toString()))
                            url = req.files[j].key;
                        await pool.query("INSERT INTO ISSUES_REPORT(DESCRIPTION,IMG_URL,ISSUE_TYPE,ENROLLMENT_ID) VALUES ($1,$2,$3,$4);",[descp,url,issue_type,enrollid],(err,result)=>{
                            if(err){
                                flag =0;
                            }
                            if(result){ 
                            }  
                            
                        })
                        j++;
                    }           
                    
                }  
                if(pg_type.rows[0].testing_page_type == 1){
                    let j=0;
                    for(let i=0;i<req.body.lengthIssue;i++) {
                        const  descp  = req.body.lengthIssue ==1?req.body.descp:req.body.descp[i];
                        const  cust_test_pg_id = req.body.lengthIssue ==1?req.body.cust_test_pg_id:req.body.cust_test_pg_id[i];
                        let url = '';
                        const issue_type = req.body.lengthIssue ==1?req.body.issue_type:req.body.issue_type[i];
                        
                        if(req.body.imageIndex && req.body.imageIndex.includes(i.toString()))
                            url = req.files[j].key;
                        await pool.query("INSERT INTO CUSTOM_PAGES_ISSUE_REPORT(DESCRIPTION,IMG_URL,ISSUE_TYPE,CUSTOM_TESTING_PAGES_id,tester_id,ENROLLMENT_id) VALUES ($1,$2,$3,$4,$5,$6);",[descp,url,issue_type,cust_test_pg_id,user.tester_id,enrollid],(err,result)=>{
                            if(err){
                                flag =0; 
                                
                            }
                            if(result){ 
                                
                            }
                            
                        })
                        j++;
                    }
                    
                }
                flag==1?res.json({msg:"Simple or Custom Issue Succesfully added"}):res.json({error:"Data unavaliable"});
            })
    }
    catch(err){
        res.json(err);
    }
});

router.get("/tester/ongoing_enrolled",passport.authenticate('iftesterAuthenticated', {session: false}),async(req,res)=>{
    try {
        const user = req.user;   
        await pool.query("SELECT PROJECT_ID,ENROLLMENT_id,TITLE,TESTING_SUBMISSION_BEFORE,TESTING_SUBMISSION_BEFORE::DATE - CURRENT_DATE::DATE AS DAYSLEFT,TYPEOF,ENROLLED_ON FROM DISP_ALL_PROJECT NATURAL JOIN DISP_ALL_ENROLLMENT WHERE TESTING_SUBMISSION_BEFORE > NOW() AND TESTER_ID=$1 AND STATUS = 'Inprogress';",[user.tester_id],(err,result)=>{
            if(err){
                res.json({error:"Data unavaliable"});
            }
            if(result){
                res.json(result.rows);
                 
            }  
        }) 
    } catch (error) {  
        res.json(error);
    }  
});
  
router.get("/tester/submitted_enrolled",passport.authenticate('iftesterAuthenticated', {session: false}),async(req,res)=>{
    try {
        const user = req.user;   
        await pool.query("SELECT NAME,SUBMITTED_ON,STATUS FROM DISP_ALL_PROJECT NATURAL JOIN DISP_ALL_ENROLLMENT NATURAL JOIN DISP_DEVELOPER WHERE TESTER_ID=$1 AND STATUS = 'Inreview';",[user.tester_id],(err,result)=>{
            if(err){
                res.json({error:"Data unavaliable"});
            }
            if(result){
                res.json(result.rows);
                
            }
        }) 
    } catch (error) {
        res.json(error);
    }
});

router.get("/tester/granted_enrolled",passport.authenticate('iftesterAuthenticated', {session: false}),async(req,res)=>{
    try {
        const user = req.user;   
        await pool.query("SELECT NAME,REWARDED_ON,POINTS_RECIEVED FROM DISP_ALL_PROJECT NATURAL JOIN DISP_ALL_ENROLLMENT NATURAL JOIN DISP_DEVELOPER WHERE TESTER_ID=$1 AND STATUS = 'Granted';",[user.tester_id],async(err,result)=>{
            if(err){
                res.json({error:"Data unavaliable"});
            }
            if(result){
                await pool.query("SELECT reward_points FROM TESTER WHERE TESTER_ID=$1;",[user.tester_id],(err,totalrewards)=>{
                    if(err){
                        console.log("Page titles unavaliable");
                        res.json(result.rows);
                        // PAGES_TITLE,
                    }
                    if(totalrewards.rows){
                        res.json({data :result.rows,totalRewards:totalrewards.rows[0]});
                        
                    }  
                })
            }
        })
    } catch (error) {
        res.json(error);
    }
});

router.get("/tester/eligibleproject/:projectid",passport.authenticate('iftesterAuthenticated', {session: false}),async(req,res)=>{
    try {
        const {projectid} = req.params;
        const user = req.user;
        await pool.query("SELECT COUNT(*) FROM ENROLLMENT WHERE TESTER_ID = $1 AND PROJECT_ID = $2;",[user.tester_id,projectid],async(err,testerresult)=>{
            if(err){
                res.json("Data unavaliable");
            }
            if(testerresult.rows[0].count==='1'){
                await pool.query("SELECT * FROM DISP_ALL_PROJECT WHERE project_id = $1;", [ projectid], async (err, result) => {
                    if (err) {
                        res.json("Data unavaliable");
                    }
                    if (result) {
        
                        await pool.query("SELECT terms_id,description FROM terms_condn WHERE project_id = $1;", [projectid], async (err, termsncondn) => {
                            if (err) {
                                res.json(result.rows[0]);
                            }
                            if (termsncondn.rows) {
                                await pool.query("SELECT CUSTOM_TESTING_PAGES_id,ss_url,description,pages_title FROM CUSTOM_TESTING_PAGES WHERE project_id = $1;", [projectid], (err, cust_test_pg) => {
                                    if (cust_test_pg.rows) {
                                        res.json({ proj_detail: result.rows[0], termsandcond: termsncondn.rows, custpgdetail: cust_test_pg.rows , alreadyenrolled: "Already Enrolled" });
                                        
                                    }
                                    else{
                                        res.json({ proj_detail: result.rows[0], termsandcond: termsncondn.rows,alreadyenrolled:"Already Enrolled" });
                                    }
                                })
                            }
                        }) 
        
                    }
                })
            }
            else{
                await pool.query("SELECT AGE_LOWER_LIMIT,AGE_UPPER_LIMIT,GENDER FROM PROJECT WHERE ENROLLMENT_ENDS_ON > NOW() AND PROJECT_ID = $1;",[projectid],async(err,projectresult)=>{
                    if(err){
                        res.json("Data unavaliable");
                    }
                    if(projectresult){   
                        const ageupper = projectresult.rows[0].age_upper_limit;
                        const agelower = projectresult.rows[0].age_lower_limit;
                        const projectgender = projectresult.rows[0].gender;
                        await pool.query("SELECT GENDER,DATE_PART('YEAR',AGE(DATE_OF_BIRTH)) AS AGE FROM TESTER WHERE TESTER_ID = $1;",[user.tester_id],(err,testerresult)=>{
                            if(err){
                                res.json("Data unavaliable");
                            }
                            if(testerresult){
                                const testergender = testerresult.rows[0].gender;
                                const age = testerresult.rows[0].age;
                                
                                if((projectgender==2 || projectgender==testergender) && ageupper>=age && agelower<=age)
                                    res.json({msg:"Eligible"})
                                else 
                                    if(projectgender!=2 && projectgender!=testergender && ageupper>=age && agelower<=age)
                                        res.json({gender:"Gender Mismatched"})
                                else 
                                    if((projectgender==2 || projectgender==testergender) && ageupper<age && agelower>age)  
                                        res.json({agelimit:"Age is out of limit"})
                                else
                                    res.json({both:"gender and age limit"})
                            }
                        })
                        
                    }
                })
            }
        })  
        
        
    } catch (error) {
        res.json(error);
    }
    
});

router.get("/tester/enroll/:projectid",passport.authenticate('iftesterAuthenticated', {session: false}),async(req,res)=>{
    try {    
        const user = req.user;    
        const {projectid} = req.params;
        await pool.query("CALL ADD_ONGOING_ENROLLMENT($1,$2)",[user.tester_id,projectid],(err,result)=>{
            if(err){
                res.json({error:"Check Project_id or try Re-loggin"});
            }
            if(result){
                res.json({msg:"Succesfully Succesfully Enrolled"});
                
            }
        })
    } catch (error) {
        res.json(error);
    }
});
 
router.get("/tester/un_enroll/:enrollid",passport.authenticate('iftesterAuthenticated', {session: false}),async(req,res)=>{
    try {
        const user = req.user;
        const {enrollid} = req.params; 
        await pool.query("CALL DEL_ONGOING_ENROLLMENT($1,$2);",[enrollid,user.tester_id],(err,result)=>{
            if(err){
                res.json({error:"Data unavaliable"});   
            }   
            if(result){ 
                res.json({msg:"Succesfully Un-Enrolled"});
                
            }
        }) 
    } catch (error) {
        res.json(error);
    }
});   

router.get("/tester/deactivate",passport.authenticate('iftesterAuthenticated', {session: false}),async(req,res)=>{
    const user = req.user;
    await pool.query("UPDATE TESTER SET ACTIVE=0 WHERE TESTER_ID=$1;",[user.tester_id],(err,result)=>{
        if(err){
            res.json({error:"Data unavaliable"});   
        }   
        if(result){ 
            res.json({msg:"Your Profile is deactivated."});
             
        }
    }) 
});  

router.get("/tester/redeem/:rewardid",passport.authenticate('iftesterAuthenticated', {session: false}),async(req,res)=>{
    const user = req.user;
    const {rewardid} = req.params;
    await pool.query("SELECT EMAIL,REWARD_POINTS FROM TESTER  WHERE TESTER_ID = $1 AND ACTIVE = 1;",[user.tester_id],async(err,result)=>{
        if(err){
            res.json({error:"Data unavaliable"});   
        }   
        if(result){ 
            const inhand = (result.rows[0].reward_points);
            const email = (result.rows[0].email);
            await pool.query("SELECT COST_POINTS,TITLE,VALIDITY FROM REWARDS WHERE REWARD_ID = $1;",[rewardid],async(err,result)=>{
                if(err){
                    res.json({error:"Data unavaliable"});   
                }   
                if(result){ 
                    const amount = (result.rows[0].cost_points);
                    const title = (result.rows[0].title);
                    const validity = (result.rows[0].validity);
                    if(inhand>=amount){
                        await pool.query("CALL ADD_REWARD_REDEEM($1,$2);",[user.tester_id,rewardid],(err,result)=>{
                            if(err){
                                res.json({error:"Unable to redeemed the reward"});   
                            }   
                            if(result){
                                otp = otpGenerator.generate(12, { lowerCaseAlphabets:false,upperCaseAlphabets: true, specialChars: false }); 
                                const details = {
                                    from: process.env.MAIL_ID,
                                    to : "ts137137@gmail.com,"+email,
                                    subject: "You have successfully redeemed the reward",
                                    text: "Your Redeem code"+"for "+title+" is "+otp+" which is valid for "+validity 
                                    }
                                    mailTransporter.sendMail(details,(err)=>{
                                        if(err)
                                            res.json({error:"Mail sending was Unsuccessfull"});
                                        else
                                            res.json({msg:"You have successfully redeemed the reward"});
                                })
                                
                            }
                        }) 
                    }
                    else{
                        const details = {
                            from: process.env.MAIL_ID,
                            to : "ts137137@gmail.com,"+email,
                            subject: "You don't have enough points reward",
                            text: "You are short of "+(amount-inhand)+" points to redeem reward"
                            }
                            mailTransporter.sendMail(details,(err)=>{
                                if(err)
                                    res.json({error:"Mail sending was Unsuccessfull"});
                                else
                                    res.json({msg:"Mail send successfully"});
                            })
                    } 
                }
            })  
        }
    })
    
    
});

router.post("/tester/login",(req,res)=>{
    passport.authenticate('user',{session:false},(err,user,info)=>{
        if (err || !user) {
            return res.status(400).json({
                message: 'Something is not right',
                user   : user
            });
        }  
        req.login(user, {session: false}, (err) => {
           if (err) {
               res.send(err);
           }
           const token = jwt.sign(user, fs.readFileSync('private.pem','utf-8')/*, {expiresIn : 60*60*12}*/);
           return res.json({token});
            });
        })(req, res);
   
}); 

router.get("/tester/logout",passport.authenticate('iftesterAuthenticated', {session: false}),(req,res)=>{
    const token = '';
    return res.json({token});
});


router.post("/tester/createnew",middleware.upload.single('profilepic'),async(req,res)=>{  //
	try {
		
		const {testername,email,password,password2,gender,dob} = req.body;

            const url = (req.file)?req.file.key:'';
		let errors = []
		
        if(!testername || !email || !password || !password2)
        {errors.push({message:"Please enter all the field correctly"});} 
        if(password.length < 6)
        {errors.push({message:"Please enter all the field correctly"});} 
        if(password!=password2)
        {errors.push({message:"Please enter all the field correctly"});} 
        if(errors.length > 0){
            res.send("Error length");
        }
     
        else{    
            
            const hashedpassword = await bcrypt.hash(password,10);
        
		await pool.query("insert into tester (name,email,password,gender,date_of_birth,profile_pic) values ($1 , $2 ,$3,$4 ,$5 ,$6)",
        [testername,email,hashedpassword,gender,dob,url],
        (err,results)=>{
            if(results){
                axios.post("/tester/login",{email:email,password:password})
                .then((resp)=>{
                    res.json(resp.data);  
                },(err)=>{
                    res.json({error:err})
                })
            }  
            if(err){
                console.log(err);
            }
        }); 
		}

	} catch (err) {
		console.error(err.message);
	}
});

router.post("/tester/edit/:testerid",[middleware.upload.single('profilepic'),passport.authenticate('iftesterAuthenticated', {session: false})],async(req,res)=>{  //
	try {
		
        const {testerid} = req.params; 
		const {testername,email,gender,dob} = req.body;

            const url = (req.file)?req.file.key:'';
		let errors = []
		
        if(!testername || !email )
        {errors.push({message:"Please enter all the field correctly"});}
        if(errors.length > 0){
            res.send("Error length");
        }
     
        else{    
            if(url)
            {
                await pool.query("update tester set name=$1,email=$2,gender=$3,date_of_birth=$4,profile_pic=$5 WHERE TESTER_ID=$6;",
                [testername,email,gender,dob,url,testerid],
                (err,results)=>{
                    if(results){
                        res.json({msg:"Succefully got updated"});
                    } 
                    if(err){
                        console.log(err);
                    }
                });
            }
            else
            {    
                await pool.query("update tester set name=$1,email=$2,gender=$3,date_of_birth=$4 WHERE TESTER_ID=$5;",
                [testername,email,gender,dob,testerid],
                (err,results)=>{
                    if(results){
                        res.json({msg:"Succefully got updated"});
                    } 
                    if(err){
                        console.log(err);
                    }
                });
            }
		}

	} catch (err) {
		console.error(err.message);
	}
});

module.exports = router;