drop table if exists reward_redeem CASCADE;
drop table if exists terms_condn CASCADE;
drop table if exists issues_report CASCADE;
drop table if exists CUSTOM_TESTING_PAGES CASCADE;
drop table if exists rewards CASCADE;
drop table if exists ENROLLMENT CASCADE;
drop table if exists developer CASCADE;
drop table if exists project CASCADE;
drop table if exists tester CASCADE;
drop table if exists CUSTOM_PAGES_ISSUE_REPORT CASCADE;

create table IF NOT EXISTS tester (
	tester_id serial primary key,
	name varchar,
	email varchar unique,
	password varchar,
	gender int,
	-- 0 - Male 
	-- 1 - Female
	date_of_birth date,
	profile_pic varchar,
	profile_creation_time timestamp default current_timestamp,
-- 	profile_creation_date date current_date,
	reward_points int default 0,
	block_count int default 0,
	CHECK (block_count BETWEEN 0 AND 3),
	active int default 1,
	CHECK (active BETWEEN 0 AND 1),				
	usertype varchar
);

--  


ALTER TABLE ONLY tester ALTER COLUMN usertype SET DEFAULT 'tester';

create table IF NOT EXISTS developer(
	developer_id serial primary key,
	name varchar,
	email varchar unique,
	password varchar,
	developer_type varchar,
	pic_url varchar,
	about_us varchar,
	profile_created_on timestamp default current_timestamp,
	active int default 1,
	CHECK (active BETWEEN 0 AND 1),	
	usertype varchar
);

ALTER TABLE ONLY developer ALTER COLUMN usertype SET DEFAULT 'developer';

--  ----------------------------------VIEWS,PROCEDURES------------------------
-- -- ****************************** TRIGGER******************
-- 	CREATE OR REPLACE FUNCTION deactivateacc()
-- 	  RETURNS TRIGGER
-- 	  LANGUAGE PLPGSQL  
-- 	  AS
-- 	  $$
-- 	  BEGIN
-- 	   UPDATE TESTER SET ACTIVE=0 WHERE TESTER_ID=NEW.TESTER_ID;
	
-- 	   RETURN NEW;
-- 	  END;
-- 	  $$;	

-- 	CREATE TRIGGER change_tester_active
-- 	AFTER INSERT OR UPDATE ON TESTER
-- 	FOR EACH ROW
-- 	WHEN (NEW.block_count=3)
-- 	EXECUTE PROCEDURE deactivateacc();


	
	
	DROP VIEW IF EXISTS DISP_TESTER;

	CREATE VIEW DISP_TESTER AS SELECT tester_id,
			name,
			email,
			password,
			date_of_birth,
			profile_pic,
			profile_creation_time,
			reward_points,
			block_count,
			CASE 
				WHEN GENDER = 0 THEN 'Male'
				WHEN GENDER = 1 THEN 'Female'
			END GENDER,
			CASE
				WHEN ACTIVE = 0 THEN 'Inactive'
				WHEN ACTIVE = 1 THEN 'Active'
			END ACTIVE
			FROM TESTER;

	DROP VIEW IF EXISTS DISP_DEVELOPER;

	CREATE VIEW DISP_DEVELOPER AS SELECT developer_id,
			name,
			email,
			password,
			developer_type,
			pic_url,
			about_us,
			profile_created_on,
			CASE
				WHEN ACTIVE = 0 THEN 'Inactive'
				WHEN ACTIVE = 1 THEN 'Active'
			END ACTIVE
			FROM DEVELOPER;
--  ----------------------------------END VIEWS,PROCEDURES------------------------

create table IF NOT EXISTS project (
	project_id serial primary key,
	title varchar,
	testing_submission_before date,
	enrollment_ends_on date,
	CREATED_ON TIMESTAMP DEFAULT current_timestamp,
	ISSUE_TYPE INT,
	CHECK (ISSUE_TYPE BETWEEN 0 AND 2),
	-- 0 - SS 
	-- 1 - REC
	-- 2 - SS AND REC
	point_rewards int,
	age_upper_limit int,
	age_lower_limit int,
	TESTING_PAGE_TYPE INT,	
	CHECK (TESTING_PAGE_TYPE BETWEEN 0 AND 1),
	-- 0 - ALL
	-- 1 - CUSTOM
	GENDER INT,
	CHECK (GENDER BETWEEN 0 AND 2),
	-- 0 - MALE 
	-- 1 - FEMALE
	-- 2 - MALE AND FEMALE
	about_task varchar,
	status INT DEFAULT 1,
	CHECK (STATUS BETWEEN 0 AND 1),
	-- 0 - INACTIVE
	-- 1 - ACTIVE
	PLATFORM INT,
	CHECK (PLATFORM BETWEEN 0 AND 6),
	-- 	  0 - ANDROID
	--    1 - IOS 
	--    2 - WEBSITE
	--    3 - ANDROID AND IOS
	--    4 - ANDROID AND WEBSITE
	--    5 - IOS AND WEBSITE
	--    6 - ANDROID, IOS AND WEBSITE
	TYPEOF INT,
	CHECK (TYPEOF BETWEEN 0 AND 2),
	-- 0 - UI 
	-- 1 - FUNCTIONALITY
	-- 2 - UI AND FUNCTIONALITY
	developer_id int,
	CONSTRAINT developer_id
      FOREIGN KEY(developer_id) 
	  REFERENCES developer(developer_id)
);



create table IF NOT EXISTS CUSTOM_TESTING_PAGES(
	CUSTOM_TESTING_PAGES_id serial primary key,
	ss_url varchar,
	description varchar,
	pages_title varchar,
	project_id int,
	constraint project_id
		foreign key (project_id)
			references project(project_id)
);

create table IF NOT EXISTS TERMS_CONDN(
	terms_id serial primary key,
	description varchar,
	project_id int,
	constraint pro_id
		foreign key(project_id)
			references project(project_id)
);
-- --------------------------------------VIEWS,PROCEDURES--------------------------------
	
	CREATE OR REPLACE PROCEDURE CREATE_PROJECT(TIT PROJECT.TITLE%TYPE,TSB PROJECT.TESTING_SUBMISSION_BEFORE%TYPE,EEO PROJECT.ENROLLMENT_ENDS_ON%TYPE,ISSUE PROJECT.ISSUE_TYPE%TYPE,POINTS PROJECT.POINT_REWARDS%TYPE,AGE_UPPER PROJECT.AGE_UPPER_LIMIT%TYPE,AGE_LOW PROJECT.AGE_LOWER_LIMIT%TYPE,TEST_TYPE PROJECT.TESTING_PAGE_TYPE%TYPE,GEN PROJECT.GENDER%TYPE,ABOUT PROJECT.ABOUT_TASK%TYPE,PLAT PROJECT.PLATFORM%TYPE,TY PROJECT.TYPEOF%TYPE,DEV PROJECT.developer_id%TYPE)
	language PLPGSQL   
	as $$
	BEGIN
		INSERT INTO PROJECT (TITLE,TESTING_SUBMISSION_BEFORE,ENROLLMENT_ENDS_ON,ISSUE_TYPE,POINT_REWARDS,AGE_UPPER_LIMIT,AGE_LOWER_LIMIT,TESTING_PAGE_TYPE,GENDER,ABOUT_TASK,PLATFORM,TYPEOF,DEVELOPER_ID) 
		VALUES (TIT,TSB,EEO,ISSUE,POINTS,AGE_UPPER,AGE_LOW,TEST_TYPE,GEN,ABOUT,PLAT,TY,DEV);
	END;
	$$;

	CREATE OR REPLACE PROCEDURE UPDATE_PROJECT(PROJ_ID PROJECT.PROJECT_ID%TYPE,TIT PROJECT.TITLE%TYPE,TSB PROJECT.TESTING_SUBMISSION_BEFORE%TYPE,EEO PROJECT.ENROLLMENT_ENDS_ON%TYPE,ISSUE PROJECT.ISSUE_TYPE%TYPE,POINTS PROJECT.POINT_REWARDS%TYPE,AGE_UPPER PROJECT.AGE_UPPER_LIMIT%TYPE,AGE_LOW PROJECT.AGE_LOWER_LIMIT%TYPE,TEST_TYPE PROJECT.TESTING_PAGE_TYPE%TYPE,GEN PROJECT.GENDER%TYPE,ABOUT PROJECT.ABOUT_TASK%TYPE,PLAT PROJECT.PLATFORM%TYPE,TY PROJECT.TYPEOF%TYPE,DEV PROJECT.developer_id%TYPE)
	language PLPGSQL   
	as $$
	BEGIN
		UPDATE PROJECT SET TITLE=TIT,TESTING_SUBMISSION_BEFORE=TSB,ENROLLMENT_ENDS_ON=EEO,ISSUE_TYPE=ISSUE,POINT_REWARDS=POINTS,AGE_UPPER_LIMIT=AGE_UPPER,AGE_LOWER_LIMIT=AGE_LOW,TESTING_PAGE_TYPE=TEST_TYPE,GENDER=GEN,ABOUT_TASK=ABOUT,PLATFORM=PLAT,TYPEOF=TY WHERE PROJECT_ID = PROJ_ID AND DEVELOPER_ID = DEV;
	END;
	$$;
	-- SELECT PROJECT_ID FROM PROJECT INTO PRO_ID WHERE CREATED_ON = current_timestamp;	

	CREATE OR REPLACE PROCEDURE ADD_CUSTOM_TESTING_PAGES(PROJ_ID PROJECT.PROJECT_ID%TYPE,SURL CUSTOM_TESTING_PAGES.ss_url%TYPE,DESCP CUSTOM_TESTING_PAGES.Description%TYPE,PG_TITLE CUSTOM_TESTING_PAGES.pages_title%TYPE)
	language PLPGSQL   
	as $$
	BEGIN
		INSERT INTO CUSTOM_TESTING_PAGES (PROJECT_ID,SS_URL,description,pages_title) VALUES(PROJ_ID,SURL,DESCP,PG_TITLE);
	END;
	$$;

	

	CREATE OR REPLACE PROCEDURE ADD_TERMS_CONDN(PROJ_ID TERMS_CONDN.PROJECT_ID%TYPE,DESCP TERMS_CONDN.DESCRIPTION%TYPE)
	language PLPGSQL   
	as $$
	BEGIN
		INSERT INTO TERMS_CONDN(description,PROJECT_ID) VALUES(DESCP,PROJ_ID);
	END;
	$$;

	DROP VIEW IF EXISTS DISP_ALL_PROJECT;

	CREATE VIEW DISP_ALL_PROJECT AS SELECT PROJECT_ID,TITLE,
			TESTING_SUBMISSION_BEFORE,
			ENROLLMENT_ENDS_ON,
			CASE
				WHEN ISSUE_TYPE = 0 THEN 'Screensort'
				WHEN ISSUE_TYPE = 1 THEN 'Recording'
				WHEN ISSUE_TYPE = 2 THEN 'Screensort and Recording'
			END ISSUE_TYPE,
			AGE_LOWER_LIMIT,
			AGE_UPPER_LIMIT,
			CASE 
				WHEN TESTING_PAGE_TYPE = 0 THEN 'All'
				WHEN TESTING_PAGE_TYPE = 1 THEN 'Custom'
			END TESTING_PAGE_TYPE,
			CASE
				WHEN GENDER = 0 THEN 'Male'
				WHEN GENDER = 1 THEN 'Female'
				WHEN GENDER = 2 THEN 'Male and Female'
			END GENDER,
			ABOUT_TASK,
			CASE 
				WHEN PLATFORM = 0 THEN 'Android'
				WHEN PLATFORM = 1 THEN 'Ios' 
				WHEN PLATFORM = 2 THEN 'Website'
				WHEN PLATFORM = 3 THEN 'Android and Ios'
				WHEN PLATFORM = 4 THEN 'Android and Website'
				WHEN PLATFORM = 5 THEN 'Ios and Website'
				WHEN PLATFORM = 6 THEN 'Android, Ios and Website'
			END PLATFORM,
			CASE 
				WHEN TYPEOF = 0 THEN 'UI'
				WHEN TYPEOF = 1 THEN 'Functionality' 
				WHEN TYPEOF = 2 THEN 'UI and Functionality'
			END TYPEOF,
			POINT_REWARDS,
			PIC_URL,
			NAME,
			about_us 
			FROM PROJECT NATURAL JOIN DEVELOPER WHERE STATUS = 1 ORDER BY CREATED_ON DESC;

	

--  ----------------------------------END VIEWS,PROCEDURES------------------------


create table IF NOT EXISTS ENROLLMENT(
	ENROLLMENT_id serial primary key,
	tester_id int,
	constraint test_id
		foreign key (tester_id)
			references tester(tester_id),
	project_id int,
	constraint proj_id
		foreign key (project_id)
			references project(project_id),
	ENROLLED_ON date DEFAULT current_date,
	SUBMITTED_ON DATE,
	REWARDED_ON DATE,
	POINTS_RECIEVED INT,
	STATUS INT DEFAULT 0
	-- 0 - INPROGRESS
	-- 1 - INREVIEW
	-- 2 - GRANTED
);

--  ----------------------------------VIEWS,PROCEDURES------------------------
DROP VIEW IF EXISTS DISP_DEV_PROJECT;

	CREATE VIEW DISP_DEV_PROJECT AS SELECT PROJECT_ID,TITLE,
			TESTING_SUBMISSION_BEFORE,
			CREATED_ON,
			ENROLLMENT_ENDS_ON,
			CASE
				WHEN ISSUE_TYPE = 0 THEN 'Screensort'
				WHEN ISSUE_TYPE = 1 THEN 'Recording'
				WHEN ISSUE_TYPE = 2 THEN 'Screensort and Recording'
			END ISSUE_TYPE,
			AGE_LOWER_LIMIT,
			AGE_UPPER_LIMIT,
			CASE 
				WHEN TESTING_PAGE_TYPE = 0 THEN 'All'
				WHEN TESTING_PAGE_TYPE = 1 THEN 'Custom'
			END TESTING_PAGE_TYPE,
			CASE
				WHEN GENDER = 0 THEN 'Male'
				WHEN GENDER = 1 THEN 'Female'
				WHEN GENDER = 2 THEN 'Male and Female'
			END GENDER,
			ABOUT_TASK,
			CASE
				WHEN STATUS = 0 THEN 'Inactive'
				WHEN STATUS = 1 THEN 'Active'
			END STATUS,
			CASE 
				WHEN PLATFORM = 0 THEN 'Android'
				WHEN PLATFORM = 1 THEN 'Ios' 
				WHEN PLATFORM = 2 THEN 'Website'
				WHEN PLATFORM = 3 THEN 'Android and Ios'
				WHEN PLATFORM = 4 THEN 'Android and Website'
				WHEN PLATFORM = 5 THEN 'Ios and Website'
				WHEN PLATFORM = 6 THEN 'Android, Ios and Website'
			END PLATFORM,
			CASE 
				WHEN TYPEOF = 0 THEN 'UI'
				WHEN TYPEOF = 1 THEN 'Functionality' 
				WHEN TYPEOF = 2 THEN 'UI and Functionality'
			END TYPEOF,
			POINT_REWARDS,
			DEVELOPER_ID
			FROM PROJECT
			ORDER BY STATUS;	


	DROP VIEW IF EXISTS DISP_ALL_ENROLLMENT;

	CREATE VIEW DISP_ALL_ENROLLMENT AS SELECT ENROLLMENT_id,TESTER_ID,PROJECT_ID,ENROLLED_ON,SUBMITTED_ON,REWARDED_ON,POINTS_RECIEVED,
			CASE 
				WHEN STATUS = 0 THEN 'Inprogress'
				WHEN STATUS = 1 THEN 'Inreview'
				WHEN STATUS = 2 THEN 'Granted'
			END STATUS
			FROM ENROLLMENT;

	CREATE OR REPLACE PROCEDURE ADD_ONGOING_ENROLLMENT(TEST_ID ENROLLMENT.TESTER_ID%TYPE,PROJ_ID ENROLLMENT.PROJECT_ID%TYPE)
	language PLPGSQL   
	as $$
	DECLARE
		COUNTING INTEGER;
		ACT INTEGER;
		COMP INTEGER;
		GEN INTEGER;
		AGE INTEGER;
		MIN_AGE INTEGER;
		MAX_AGE INTEGER;
	BEGIN
		SELECT COUNT(*) INTO COUNTING FROM ENROLLMENT  WHERE PROJECT_ID = PROJ_ID AND TESTER_ID = TEST_ID;
		SELECT COUNT(*) INTO ACT FROM TESTER WHERE TESTER_ID=TEST_ID AND ACTIVE=1;
		SELECT COUNT(*) INTO COMP FROM PROJECT WHERE ENROLLMENT_ENDS_ON>NOW() AND PROJECT_ID=PROJ_ID AND GENDER = (SELECT GENDER FROM TESTER WHERE TESTER_ID = TEST_ID);
		SELECT GENDER INTO GEN FROM PROJECT WHERE ENROLLMENT_ENDS_ON>NOW() AND PROJECT_ID=PROJ_ID;
		SELECT DATE_PART('YEAR',AGE(DATE_OF_BIRTH)) INTO AGE FROM TESTER WHERE TESTER_ID = TEST_ID;
		SELECT AGE_UPPER_LIMIT INTO MAX_AGE FROM PROJECT WHERE PROJECT_ID = PROJ_ID;
		SELECT AGE_LOWER_LIMIT INTO MIN_AGE FROM PROJECT WHERE PROJECT_ID = PROJ_ID;
		IF COUNTING=0 AND ACT=1 AND AGE >= MIN_AGE AND AGE <= MAX_AGE AND (COMP=1 OR GEN=2) THEN 
			INSERT INTO ENROLLMENT (TESTER_ID,PROJECT_ID) VALUES (TEST_ID,PROJ_ID);
		ELSE
			RAISE NOTICE 'ALREADY ENROLLED OR CANT ENROLL';
		END IF;
	END;
	$$;

	CREATE OR REPLACE PROCEDURE DEL_ONGOING_ENROLLMENT(ENR_ID ENROLLMENT.ENROLLMENT_ID%TYPE,TEST_ID ENROLLMENT.TESTER_ID%TYPE)
	language PLPGSQL   
	as $$
	DECLARE
		COUNTING INTEGER;
	BEGIN
		SELECT COUNT(*) INTO COUNTING FROM ENROLLMENT  WHERE ENROLLMENT_ID = ENR_ID AND TESTER_ID = TEST_ID AND STATUS = 0;
		IF COUNTING>0 THEN 
			DELETE FROM ISSUES_REPORT WHERE ENROLLMENT_ID = ENR_ID;
			DELETE FROM ENROLLMENT WHERE ENROLLMENT_ID = ENR_ID AND TESTER_ID = TEST_ID AND STATUS = 0;
		END IF;
	END;
	$$;


--  ----------------------------------END VIEWS,PROCEDURES------------------------

create table IF NOT EXISTS issues_report(
	issue_id serial primary key,
	description varchar,
	img_url varchar,
	ISSUE_TYPE INT,
	CHECK (ISSUE_TYPE BETWEEN 0 AND 1),
	-- 0 - UI TESTING
	-- 1 - FUNCTIONALITY TESTING
	ENROLLMENT_id int,
	constraint ENROLL_id
		foreign key (ENROLLMENT_id)
			references ENROLLMENT(ENROLLMENT_id)
);

--  ----------------------------------VIEWS,PROCEDURES------------------------

	CREATE OR REPLACE PROCEDURE ADD_ISSUES_REPORT(DESCP issues_report.description%TYPE,IMGURL issues_report.img_url%TYPE,ISSTYPE issues_report.ISSUE_TYPE%TYPE,ENROLL_ID issues_report.ENROLLMENT_id%TYPE)
	language PLPGSQL   
	as $$
	DECLARE
		BOT INTEGER;
		UI INTEGER;
		FUNC INTEGER;
		ACT INTEGER;
	BEGIN
		SELECT COUNT(*) INTO BOT FROM PROJECT P JOIN ENROLLMENT E ON P.PROJECT_ID=E.PROJECT_ID WHERE ENROLLMENT_ID=ENROLL_ID AND P.TYPEOF=2;
		SELECT COUNT(*) INTO FUNC FROM PROJECT P JOIN ENROLLMENT E ON P.PROJECT_ID=E.PROJECT_ID WHERE ENROLLMENT_ID=ENROLL_ID AND P.TYPEOF=1;
		SELECT COUNT(*) INTO UI FROM PROJECT P JOIN ENROLLMENT E ON P.PROJECT_ID=E.PROJECT_ID WHERE ENROLLMENT_ID=ENROLL_ID AND P.TYPEOF=0;
		SELECT COUNT(*) INTO ACT FROM TESTER WHERE TESTER_ID=(SELECT TESTER_ID FROM ENROLLMENT WHERE ENROLLMENT_ID=ENROLL_ID) AND ACTIVE=1;
		IF BOT=1 AND ACT=1 THEN 
			INSERT INTO ISSUES_REPORT(DESCRIPTION,IMG_URL,ISSUE_TYPE,ENROLLMENT_ID) VALUES  (DESCP,IMGURL,ISSTYPE,ENROLL_ID);
		ELSE
			IF FUNC=1 AND ISSTYPE=1 AND ACT=1 THEN
				INSERT INTO ISSUES_REPORT(DESCRIPTION,IMG_URL,ISSUE_TYPE,ENROLLMENT_ID) VALUES  (DESCP,IMGURL,ISSTYPE,ENROLL_ID);
			ELSE
				IF UI=1 AND ISSTYPE=0 AND ACT=1 THEN
					INSERT INTO ISSUES_REPORT(DESCRIPTION,IMG_URL,ISSUE_TYPE,ENROLLMENT_ID) VALUES  (DESCP,IMGURL,ISSTYPE,ENROLL_ID);
				END IF;
			END IF;	
		END IF;
	END;
	$$;

	DROP VIEW IF EXISTS DISP_ALL_ISSUES_REPORT;

	CREATE VIEW DISP_ALL_ISSUES_REPORT AS SELECT ISSUE_id,description,IMG_URL,ENROLLMENT_ID,
			CASE 
				WHEN ISSUE_TYPE = 0 THEN 'UI Testing'
				WHEN ISSUE_TYPE = 1 THEN 'Functionality Testing'
			END ISSUE_TYPE
			FROM ISSUES_REPORT;


--  ----------------------------------END VIEWS,PROCEDURES------------------------

CREATE TABLE IF NOT EXISTS CUSTOM_PAGES_ISSUE_REPORT(
	CUST_ISSUE_ID SERIAL PRIMARY KEY,
	description varchar,
	img_url varchar,
	ISSUE_TYPE INT,
	CHECK (ISSUE_TYPE BETWEEN 0 AND 1),
	-- 0 - UI TESTING
	-- 1 - FUNCTIONALITY TESTING
	CUSTOM_TESTING_PAGES_id INT,
	constraint CUST_TEST_PG_id
		foreign key (CUSTOM_TESTING_PAGES_id)
			references CUSTOM_TESTING_PAGES(CUSTOM_TESTING_PAGES_id),
	tester_id int,
	CONSTRAINT tester_id
      FOREIGN KEY(tester_id) 
	  REFERENCES tester(tester_id),
	ENROLLMENT_id int,
	constraint ENROLL_id
		foreign key (ENROLLMENT_id)
			references ENROLLMENT(ENROLLMENT_id)
		
);

--  ----------------------------------VIEWS,PROCEDURES------------------------
	CREATE OR REPLACE PROCEDURE DEL_CUSTOM_TESTING_PAGES (CUST_PG_ID CUSTOM_TESTING_PAGES.CUSTOM_TESTING_PAGES_id%TYPE)
	LANGUAGE SQL
	AS $$
	DELETE FROM CUSTOM_PAGES_ISSUE_REPORT WHERE CUSTOM_TESTING_PAGES_id = CUST_PG_ID;
	DELETE FROM CUSTOM_TESTING_PAGES WHERE CUSTOM_TESTING_PAGES_id = CUST_PG_ID;
	$$;


	CREATE OR REPLACE PROCEDURE ADD_CUSTOM_PAGES_ISSUE_REPORT(DESCP CUSTOM_PAGES_ISSUE_REPORT.description%TYPE,IMGURL CUSTOM_PAGES_ISSUE_REPORT.img_url%TYPE,ISSTYPE CUSTOM_PAGES_ISSUE_REPORT.ISSUE_TYPE%TYPE,CUST_TEST_PG_id CUSTOM_PAGES_ISSUE_REPORT.CUSTOM_TESTING_PAGES_id%TYPE,TEST_ID CUSTOM_PAGES_ISSUE_REPORT.tester_id%TYPE,ENROLL_ID CUSTOM_PAGES_ISSUE_REPORT.ENROLLMENT_id%TYPE)
	language PLPGSQL   
	as $$
	DECLARE
		BOT INTEGER;
		UI INTEGER;
		FUNC INTEGER;
		ACT INTEGER;
	BEGIN
		SELECT COUNT(*) INTO BOT FROM PROJECT P JOIN ENROLLMENT E ON P.PROJECT_ID=E.PROJECT_ID WHERE ENROLLMENT_ID=ENROLL_ID AND TESTER_ID = TEST_ID AND P.TYPEOF=2;
		SELECT COUNT(*) INTO FUNC FROM PROJECT P JOIN ENROLLMENT E ON P.PROJECT_ID=E.PROJECT_ID WHERE ENROLLMENT_ID=ENROLL_ID AND TESTER_ID = TEST_ID AND P.TYPEOF=1;
		SELECT COUNT(*) INTO UI FROM PROJECT P JOIN ENROLLMENT E ON P.PROJECT_ID=E.PROJECT_ID WHERE ENROLLMENT_ID=ENROLL_ID AND TESTER_ID = TEST_ID AND P.TYPEOF=0;
		SELECT COUNT(*) INTO ACT FROM TESTER WHERE TESTER_ID=TEST_ID AND ACTIVE=1;
		IF BOT=1 AND ACT=1 THEN 
			INSERT INTO CUSTOM_PAGES_ISSUE_REPORT(DESCRIPTION,IMG_URL,ISSUE_TYPE,CUSTOM_TESTING_PAGES_id,tester_id,ENROLLMENT_id) VALUES (DESCP,IMGURL,ISSTYPE,CUST_TEST_PG_id,TEST_ID,ENROLL_ID);
		ELSE
			IF FUNC=1 AND ISSTYPE=1 AND ACT=1 THEN
				INSERT INTO CUSTOM_PAGES_ISSUE_REPORT(DESCRIPTION,IMG_URL,ISSUE_TYPE,CUSTOM_TESTING_PAGES_id,tester_id,ENROLLMENT_id) VALUES (DESCP,IMGURL,ISSTYPE,CUST_TEST_PG_id,TEST_ID,ENROLL_ID);
			ELSE
				IF UI=1 AND ISSTYPE=0 AND ACT=1 THEN
					INSERT INTO CUSTOM_PAGES_ISSUE_REPORT(DESCRIPTION,IMG_URL,ISSUE_TYPE,CUSTOM_TESTING_PAGES_id,tester_id,ENROLLMENT_id) VALUES (DESCP,IMGURL,ISSTYPE,CUST_TEST_PG_id,TEST_ID,ENROLL_ID);
				END IF;
			END IF;	
		END IF;
	END;
	$$;

	DROP VIEW IF EXISTS DISP_ALL_CUST_PG_ISS_REP;

	CREATE VIEW DISP_ALL_CUST_PG_ISS_REP AS SELECT CUST_ISSUE_ID,description,IMG_URL,tester_id,CUSTOM_TESTING_PAGES_id,ENROLLMENT_id,
			CASE 
				WHEN ISSUE_TYPE = 0 THEN 'UI Testing'
				WHEN ISSUE_TYPE = 1 THEN 'Functionality Testing'
			END ISSUE_TYPE
			FROM CUSTOM_PAGES_ISSUE_REPORT;

-- ****************************** TRIGGER******************

CREATE OR REPLACE FUNCTION enrollment_status_change()
  RETURNS TRIGGER 
  LANGUAGE PLPGSQL  
  AS
$$
BEGIN
	UPDATE ENROLLMENT SET STATUS = 1,SUBMITTED_ON = current_date WHERE ENROLLMENT_id=NEW.ENROLLMENT_id;
  
	RETURN NEW;
END;
$$;

CREATE TRIGGER change_enrollment_status
  AFTER INSERT 
  ON issues_report
  FOR EACH ROW
  EXECUTE PROCEDURE enrollment_status_change();



CREATE TRIGGER change_cust_enrollment_status
  AFTER INSERT 
  ON CUSTOM_PAGES_ISSUE_REPORT
  FOR EACH ROW
  EXECUTE PROCEDURE enrollment_status_change();



--  ----------------------------------END VIEWS,PROCEDURES------------------------

create table IF NOT EXISTS rewards(
	reward_id serial primary key,
	title varchar,
	cost_points int,
	img_url varchar,
	validity varchar,
	rewardtype int,
	CHECK (rewardtype BETWEEN 0 AND 3)

);

create table IF NOT EXISTS reward_redeem(
	redeem_id serial primary key,
	tester_id int,
	constraint tester_id
		foreign key (tester_id)
			references tester(tester_id),
	reward_id int,
	constraint reward_id
		foreign key (reward_id)
			references rewards(reward_id),
	redeem_on timestamp default current_timestamp
);

--  ----------------------------------VIEWS,PROCEDURES------------------------

CREATE OR REPLACE PROCEDURE ADD_REWARD_REDEEM(TEST_ID REWARD_REDEEM.TESTER_ID%TYPE,REW_ID REWARD_REDEEM.REWARD_ID%TYPE)
	language PLPGSQL   
	as $$
	DECLARE
		AMOUNT INTEGER;
		INHAND INTEGER;
	BEGIN
		SELECT REWARD_POINTS INTO INHAND FROM TESTER  WHERE TESTER_ID = TEST_ID AND ACTIVE = 1;
		SELECT COST_POINTS INTO AMOUNT FROM REWARDS WHERE REWARD_ID = REW_ID;
		IF INHAND>=AMOUNT THEN 
			INSERT INTO REWARD_REDEEM(TESTER_ID,REWARD_ID) VALUES (TEST_ID,REW_ID);
			UPDATE TESTER SET REWARD_POINTS = INHAND - AMOUNT WHERE TESTER_ID = TEST_ID;
		ELSE
			RAISE NOTICE 'NOT ENOUGH POINTS';
		END IF;
	END;
	$$;
 

-- testers
insert into tester (name,email,password,gender,date_of_birth,profile_pic) 
values ('LillRobby' , 'LillRobby@gmail.com' ,'$2b$10$sGZHQrk3LorZ5f54kYE2EOx.Gf78rypC0C6uw7LEGhW3MPaErQID2',0,'1990-01-10' ,'elon-musk.jpg');

-- insert into tester (name,email,password,gender,date_of_birth,profile_pic) 
-- values ('LillRobby' , 'ts137137@gmail.com' ,'$2b$10$sGZHQrk3LorZ5f54kYE2EOx.Gf78rypC0C6uw7LEGhW3MPaErQID2',0,'2022-01-10' ,'not_defined_yet');

insert into tester (name,email,password,gender,date_of_birth,profile_pic) 
values ('Lorena Bailey',	'Lorena.Bailey68@hotmail.com',	'$2b$10$ajIzgYzrFgLCmKeLHDqIeu7KkH2Ct3lBLwTqkZ43z6C0BMmymS/IO',	1,	'1990-10-21',	'elon-musk.jpg');

insert into tester (name,email,password,gender,date_of_birth,profile_pic) 
values ('Kassandra Lebsack',	'Kassandra_Lebsack@yahoo.com',	'$2b$10$kMEF1eBdGpvwp7qhxEFDjuyNfZSv/sRO9U9FJD.gPnmhhYeeIAkGK',	1,	'1999-03-10',	'elon-musk.jpg');

insert into tester (name,email,password,gender,date_of_birth,profile_pic) 
values ('James Roob',	'Jameson.Waelchi42@hotmail.com'	,'$2b$10$Qp2TWDXpBRVwzGEvpSchG.WdVJrOx1gVyNhJ9NXBIZg6BoPHDa0Oa',	0	,'1972-02-10',	'elon-musk.jpg');

insert into tester (name,email,password,gender,date_of_birth,profile_pic) 
values ('Warren Mayer Jr.',	'Warren_Mayer_Jr.@hotmail.com',	'$2b$10$h7MqFNvdpCho8Ajkr6Cs9.icJxLzBJGU2e/462oLys7JkhlzyUCw6',	0,	'1982-12-19',	'elon-musk.jpg');

-- developers
insert into developer (name,email,password,developer_type,pic_url,about_us) 
values ('Rickey Rempel' , 'RickeyRempel@gmail.com','$2b$10$EjLrRRi2Myfn0G9WxXmuwuTUs2GqwQXzQzSZHPRKQqrGesJ3Gpxlm','Freelancer','microsoft.jpg','Assumenda dolore voluptatem dolores minima ut officiis ist…e laboriosam et aliquam laborum odio odit. Id iste nihil.');

insert into developer (name,email,password,developer_type,pic_url,about_us) 
values ('Lavon Kertzmann'	,'Lavon_Kertzmann55@gmail.com',	'$2b$10$1umXy7AUBUo4kFR9jQcBOOUBjt2tcmpr/Wbn4lYNwddy2hi0qRJaG',	'Dynamic',	'Delta-Logo.png',	'somedesciption');

insert into developer (name,email,password,developer_type,pic_url,about_us) 
values ('Kitty Wolff'	,'Kitty_Wolff@hotmail.com',	'$2b$10$EF84Aj/1mGUK8J0iNcxtduamnQx2GsvSe99wNhq0Bj2JR/xYu.KbW',	'Lead',	'facebook.png',	'somedesciption');

insert into developer (name,email,password,developer_type,pic_url,about_us) 
values ('Pollich - Herman',	'ContactUs@Pollich-Herman.com',	'$2b$10$cdkJ7roeyf7p7brfAQeL3O5ZssaIUcsP.wERi2VkEOri5iCBLdeWa',	'International'	,'google.png',	'somedesciption');

insert into developer (name,email,password,developer_type,pic_url,about_us) 
values ('Collins and Sons',	'ContactUs@CollinsAndSons.com',	'$2b$10$.8eGKWtxJjSLQ1AmPU6icugg2YhM5IVRBx6vF3uLqKZyoBOQT4ivy',	'District',	'tataneu.jpeg',	'somedesciption');



-- projects

CALL CREATE_PROJECT('Microsoft Android Application Testing', '2023-02-25'::DATE,'2023-02-15'::DATE,0,100,100,15,1,0,'Ut corporis facilis. Dolor iure quis qui accusamus quaerat. Quam illum expedita omnis nobis nesciunt aliquam. Qui animi impedit doloremque sapiente eum eos commodi.',1,2,1);

CALL CREATE_PROJECT('Delta Website Testing', '2023-01-21'::DATE,'2023-01-15'::DATE,0,100,100,15,0,1,'Rem laudantium et qui explicabo doloremque sed necessitatibus numquam. Officiis tempore ut maiores necessitatibus officia perspiciatis omnis et quia. Et autem nemo sit sit.',0,1,2);

CALL CREATE_PROJECT('Facebook iOS Application Testing', '2023-05-12'::DATE,'2023-04-15'::DATE,0,75,100,12,0,2,'Dignissimos omnis doloribus eum est totam.',1,2,3);

CALL CREATE_PROJECT('Google GameDay Android','2023-08-11'::DATE,'2023-07-30'::DATE,1,120,100,20,1,2,'Quisquam praesentium omnis. Laborum voluptatem error incid…est. Dolore aspernatur ab quia et amet tenetur qui dicta.',3,2,4);

CALL CREATE_PROJECT('TataNeu Website','2023-10-08'::DATE,'2023-10-01'::DATE,2,100,100,20,1,2,'Quos et provident quis sit excepturi id facilis aliquid. E…Recusandae esse repudiandae et quo quo qui dolor quaerat.',6,0,5);

-- Custom pages of Projects
CALL ADD_CUSTOM_TESTING_PAGES(1,'1648405913118-Screenshot from 2021-10-15 20-33-32.png','SOME DESCRIPTION','HOME PAGE');
CALL ADD_CUSTOM_TESTING_PAGES(1,'1648405913124-Screenshot from 2021-10-15 20-34-06.png','SOME DESCRIPTION','ABOUT US PAGE');
CALL ADD_CUSTOM_TESTING_PAGES(1,'1648405913149-Screenshot from 2021-10-15 20-34-25.png','SOME DESCRIPTION','LOGIN PAGE');

CALL ADD_CUSTOM_TESTING_PAGES(4,'1648405913118-Screenshot from 2021-10-15 20-33-32.png','SOME DESCRIPTION','HOME PAGE');
CALL ADD_CUSTOM_TESTING_PAGES(4,'1648405913124-Screenshot from 2021-10-15 20-34-06.png','SOME DESCRIPTION','ABOUT US PAGE');
CALL ADD_CUSTOM_TESTING_PAGES(4,'1648405913149-Screenshot from 2021-10-15 20-34-25.png','SOME DESCRIPTION','LOGIN PAGE');

CALL ADD_CUSTOM_TESTING_PAGES(5,'1648405913118-Screenshot from 2021-10-15 20-33-32.png','SOME DESCRIPTION','HOME PAGE');
CALL ADD_CUSTOM_TESTING_PAGES(5,'1648405913124-Screenshot from 2021-10-15 20-34-06.png','SOME DESCRIPTION','ABOUT US PAGE');
CALL ADD_CUSTOM_TESTING_PAGES(5,'1648405913149-Screenshot from 2021-10-15 20-34-25.png','SOME DESCRIPTION','LOGIN PAGE');


-- ENROLLMENT

CALL ADD_ONGOING_ENROLLMENT(2,2);
CALL ADD_ONGOING_ENROLLMENT(3,3);
CALL ADD_ONGOING_ENROLLMENT(1,3);
CALL ADD_ONGOING_ENROLLMENT(5,1);


CALL ADD_ONGOING_ENROLLMENT(1,1);
CALL ADD_ONGOING_ENROLLMENT(3,2);
CALL ADD_ONGOING_ENROLLMENT(4,4);
CALL ADD_ONGOING_ENROLLMENT(5,5);
CALL ADD_ONGOING_ENROLLMENT(1,4);

-- ONLY ENROLLMENT
CALL ADD_ONGOING_ENROLLMENT(1,5);

-- CUSTOM ISSUE REPORTED BY TESTER 1 TO PROJECT 1
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTS1.1','1648405913118-Screenshot from 2021-10-15 20-33-32.png',0,1,1,5);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTS1.1','1648405913118-Screenshot from 2021-10-15 20-33-32.png',0,1,1,5);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTS2.1','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,2,1,5);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTS2.2','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,2,1,5);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTS3','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,3,1,5);


-- CUSTOM ISSUE REPORTED BY TESTER 4 TO PROJECT 4
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTSA.1','1648405913118-Screenshot from 2021-10-15 20-33-32.png',0,1,4,7);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTSA.1','1648405913118-Screenshot from 2021-10-15 20-33-32.png',0,1,4,7);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTSB.1','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,2,4,7);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTSB.2','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,3,4,7);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTSC','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,3,4,7);

-- CUSTOM ISSUE REPORTED BY TESTER 5 TO PROJECT 5
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTSA.1','1648405913118-Screenshot from 2021-10-15 20-33-32.png',0,1,5,8);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTSA.1','1648405913118-Screenshot from 2021-10-15 20-33-32.png',0,1,5,8);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTSB.1','1648405913118-Screenshot from 2021-10-15 20-33-32.png',0,2,5,8);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTSB.2','1648405913118-Screenshot from 2021-10-15 20-33-32.png',0,3,5,8);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTSC','1648405913118-Screenshot from 2021-10-15 20-33-32.png',0,3,5,8);

-- CUSTOM ISSUE REPORTED BY TESTER 1 TO PROJECT 4
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTSA.1','1648405913118-Screenshot from 2021-10-15 20-33-32.png',0,1,1,9);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTSA.1','1648405913118-Screenshot from 2021-10-15 20-33-32.png',0,1,1,9);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTSB.1','1648405913118-Screenshot from 2021-10-15 20-33-32.png',0,2,1,9);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTSB.2','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,3,1,9);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT ('sOME cOMMENTSC','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,3,1,9);

-- ISSUE REPORTED BY TESTER 5 TO PROJECT 1
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT('My Report or comment about project1','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,1,5,4);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT('My Report or comment about project2','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,2,5,4);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT('My Report or comment about project4','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,3,5,4);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT('My Report or comment about project4','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,1,5,4);
CALL ADD_CUSTOM_PAGES_ISSUE_REPORT('My Report or comment about project4','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,2,5,4);
	
-- ISSUE REPORTED BY TESTER 3 TO PROJECT 2
CALL ADD_ISSUES_REPORT ('sOME cOMMENTSA.1','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,6);
CALL ADD_ISSUES_REPORT ('sOME cOMMENTSA.1','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,6);
CALL ADD_ISSUES_REPORT ('sOME cOMMENTSB.1','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,6);
CALL ADD_ISSUES_REPORT ('sOME cOMMENTSB.2','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,6);
CALL ADD_ISSUES_REPORT ('sOME cOMMENTSC.2','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,6);

-- ISSUE REPORTED BY TESTER 1 TO PROJECT 3
CALL ADD_ISSUES_REPORT('My Report or comment about project1','1648405913118-Screenshot from 2021-10-15 20-33-32.png',0,3);
CALL ADD_ISSUES_REPORT('My Report or comment about project2','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,3);
CALL ADD_ISSUES_REPORT('My Report or comment about project3','1648405913118-Screenshot from 2021-10-15 20-33-32.png',0,3);
CALL ADD_ISSUES_REPORT('My Report or comment about project3','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,3);
CALL ADD_ISSUES_REPORT('My Report or comment about project3','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,3);

-- ISSUE REPORTED BY TESTER 3 TO PROJECT 3
CALL ADD_ISSUES_REPORT('My Report or comment about project1','1648405913118-Screenshot from 2021-10-15 20-33-32.png',0,2);
CALL ADD_ISSUES_REPORT('My Report or comment about project2','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,2);
CALL ADD_ISSUES_REPORT('My Report or comment about project2','1648405913118-Screenshot from 2021-10-15 20-33-32.png',0,2);
CALL ADD_ISSUES_REPORT('My Report or comment about project2','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,2);
CALL ADD_ISSUES_REPORT('My Report or comment about project2','1648405913118-Screenshot from 2021-10-15 20-33-32.png',1,2);


-- reward with 50 points to redeem
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Levi''s Clothing' , 100, 'levis.png','2 Months',0);
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Gucci Party Dress - Female' , 200, 'gucci.jpg','1 year',0);
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Louis Vitton Clothing' , 600,'lv.jpg','1 year',0);
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Chanel Bags' , 200,'Chanel.png','2 year',0);
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Nike Sport Trousers' , 550,'nike.jpg','6 Months',0);
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Adidas Sports T-shirt' , 290,'Adidas.jpg','1 year',0);
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Polo Tshirts' , 499,'polo.jpg','1 year',0);
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Flipkart Grocery' , 100,'Flipkart.png','1 year',1);
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Amazon Basics' , 299,'img-amazon.png','2 years',1);
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Myntra Hand Bags' , 800,'myntra.jfif','1 year',1);
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Marvel Hoodies' , 499,'sstore.png','6 Months',1);
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Amazon Prime' , 899,'img-amazon.png','1 year',1);
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Amazon Books' , 256,'img-amazon.png','1 year',1);
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Dominos Combo Pizza' , 489,'dominos.png','1 year',2);
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Pizza Hut Veg Pizza' , 699,'Hut.jpg','1 year',2);
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Fassos Franky' , 499,'Faasos.png','1 year',2);
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Burger King' , 399,'bk.jfif','2 years',2);
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Paytm Headphones' , 799,'paytm.jpg','1 year',3);
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Lenskart Glasses' , 999,'lenskart.jpg','1 year',3);	
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Amazon Electronics' , 1599,'img-amazon.png','1 year',3);	
insert into rewards (title,cost_points,img_url,validity,rewardtype)
	values ('Disney + HotStar Premium Membership' , 1999,'hotstar.jpg','1 year',3);														
	


-- terms and condn for project 1
CALL ADD_TERMS_CONDN (1,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (1,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (1,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (1,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (1,'Best of Luck (Description)');

CALL ADD_TERMS_CONDN (2,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (2,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (2,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (2,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (2,'Best of Luck (Description)');

CALL ADD_TERMS_CONDN (3,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (3,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (3,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (3,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (3,'Best of Luck (Description)');

CALL ADD_TERMS_CONDN (4,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (4,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (4,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (4,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (4,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (4,'Best of Luck (Description)');

CALL ADD_TERMS_CONDN (5,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (5,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (5,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (5,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (5,'Best of Luck (Description)');
CALL ADD_TERMS_CONDN (5,'Best of Luck (Description)');