const nodemailer = require("nodemailer");

const mailTransporter = nodemailer.createTransport({
    service: "gmail",
    auth:{
        user: process.env.MAIL_ID,
        pass: process.env.MAIL_PASSWORD
    }
})


module.exports = mailTransporter;