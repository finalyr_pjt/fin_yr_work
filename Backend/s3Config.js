const S3 = require('aws-sdk/clients/s3')

const s3Object = {}
const bucket = process.env.AWS_BUCKET_NAME
const region = process.env.AWS_BUCKET_REGION
const accessKeyId = process.env.AWS_ACCESS_KEY
const secretAccessKey = process.env.AWS_SECRET_KEY

s3Object.s3 = new S3({
    region,
    accessKeyId,
    secretAccessKey
})

s3Object.getFileStream = (fileKey)=>{
    const downloadParams = {
        Key: fileKey,
        Bucket: bucket
    }
    return s3Object.s3.getObject(downloadParams).createReadStream()
} 

module.exports = s3Object;