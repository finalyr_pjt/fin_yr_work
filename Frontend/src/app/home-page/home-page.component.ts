import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import AOS from 'aos';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  homeProjects: any;
  project_id: any;
  apiURL = environment.apiURL;
  constructor(private router:Router,private http: HttpClient) { }

  ngOnInit(){
    this.http.get(this.apiURL+'/')
    .subscribe((homeProjects)=> {
      this.homeProjects=homeProjects;
    })

    AOS.init(); 
  }

  navigatetoexplore(){
    this.router.navigate(['/explore']);
  };

  navigatetosignin(){
    this.router.navigate(['/login-signup']);
  }

  projectdetails(project_id){
    this.router.navigate(['/project-task-details/'+project_id]);
  }

  categorylist(){
    this.router.navigate(['/explore'],{fragment: 'iOS'});
  }

  developerlogin(){
    this.router.navigate(['/developer-login']);
  }


  

}
