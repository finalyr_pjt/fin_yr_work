import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.css']
})
export class TestingComponent implements OnInit {
  apiURL = environment.apiURL;
  createtask!: FormGroup;
  

  constructor(
    private router: Router,
    // private location: Location,
    private http: HttpClient,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.createtask = this.fb.group({
      title: [''],
      custpgRows: this.fb.array([this.initCustpgRows()])
      
    });
  }
  initCustpgRows() {
      
      return this.fb.group({
        images: [''],
        descp: ['']
      })
    
  }
  // onFileSelect1(event) {
  //   if (event.target.files.length > 0) {
  //     const file = event.target.files[0];
  //     this.createtask.value. = file;
  //   }
  // }
  onFileSelect(event,i) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.createtask.controls['custpgRows']['controls'][i].value.images = file;
    }
  }
  get formArr() {
    return this.createtask.get('termRows') as FormArray;
  }

  get formCustArr() {
    return this.createtask.get('custpgRows') as FormArray;
  }

  get f() {
    return this.createtask.controls;
  }
  addNewCustpg(t?: any) {
    this.formCustArr.push(this.initCustpgRows());
  }
  deleteRow(index: number) {
    this.formArr.removeAt(index);
  }
  deleteCustpgRow(index: number) {
    this.formCustArr.removeAt(index);
  }
  onSubmit() {
    const formData = new FormData();
    let images:any =[];
    for ( let cust of this.createtask.value.custpgRows){
      formData.append('images', cust.images);
      
    }
    // formData.append('images', this.createtask.value.custpgRows[0].images);
    // formData.append('images', this.createtask.value.custpgRows[1].images);
    this.http.post<any>(this.apiURL+'/dev/createnewprojectdemo', formData).subscribe(result => {
      if (result.msg)
        this.router.navigate(['/'])
    })
}
}
