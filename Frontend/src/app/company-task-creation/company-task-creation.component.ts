import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe, Location } from '@angular/common';
import { HttpClient, HttpEventType, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment.prod';
@Component({
  selector: 'app-company-task-creation',
  templateUrl: './company-task-creation.component.html',
  styleUrls: ['./company-task-creation.component.css']
})
export class CompanyTaskCreationComponent implements OnInit {

  reqHeader = new HttpHeaders({ 
    'Authorization': 'Bearer ' + (localStorage.getItem('token'))
  });
  project_id: any;
  projectDetail: any;
  selectedFiles:File[]= [];
  imageIndex:any[]= [];
  apiURL = environment.apiURL;
  user:any;
  submitted: any;
  progress: any;
  currentDate :any;
  constructor(
    private router: Router,
    private location: Location,
    private http: HttpClient,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private datePipe: DatePipe
  ) { }


  formpages: number = 0;
  createtask!: FormGroup;

  ngOnInit(): void {
    var date = new Date();
    this.currentDate = this.datePipe.transform(date,"yyyy-MM-dd");
    this.user = localStorage.getItem('user');
    this.formpages = 0;
    this.createtask = this.fb.group({
      title: ['',Validators.required],
      tsb: ['',Validators.required],
      eeo: ['',Validators.required],
      iss_type: [''],
      points: ['',Validators.required],
      age_upper: ['',Validators.required],
      age_lower: ['',Validators.required],
      test_type: ['',Validators.required],
      custpgRows: this.fb.array([this.initCustpgRows()]),
      gen: ['',Validators.required],
      about: [''],
      plaform: ['',Validators.required],
      type: ['',Validators.required],
      termRows: this.fb.array([this.initTermRows()])
    });

    this.route.params.subscribe(params => {
      this.project_id = params['project_id'];

    });

    try {
      if (this.project_id)
      this.getProject(this.project_id);
    } catch (error) {
      console.log(error);
    }
   
  }

  private getProject(project_id: number) {
    if (project_id == 0) {

    }
    else {
      this.http.get(this.apiURL+'/dev/updateproject/' + this.project_id, { headers: this.reqHeader })
        .subscribe((projectDetail) => {
          this.formpages = projectDetail['proj_detail']['testing_page_type']
          const terms = projectDetail['termsandcond']
          this.deleteRow(0,'');
          terms.forEach(t => {
            this.addNewIssue(t)
          });
          const custpg = projectDetail['custpgdetail']
          this.deleteCustpgRow(0,'');
          custpg.forEach(t => {
            this.addNewCustpg(t)
          });
          this.createtask.patchValue({
            title: projectDetail['proj_detail']['title'],
            tsb: projectDetail['proj_detail']['testing_submission_before'],
            eeo: projectDetail['proj_detail']['enrollment_ends_on'],
            iss_type: projectDetail['proj_detail']['issue_type'],
            points: projectDetail['proj_detail']['point_rewards'],
            age_upper: projectDetail['proj_detail']['age_upper_limit'],
            age_lower: projectDetail['proj_detail']['age_lower_limit'],
            test_type: projectDetail['proj_detail']['testing_page_type'],
            gen: projectDetail['proj_detail']['gender'],
            about: projectDetail['proj_detail']['about_task'],
            plaform: projectDetail['proj_detail']['platform'],
            type: projectDetail['proj_detail']['typeof'],
            custpgRows: {
              custid: (projectDetail['custpgdetail'].map(({ custom_testing_pages_id }) => custom_testing_pages_id)),
              images: (projectDetail['custpgdetail'].map(({ ss_url }) => ss_url)),
              pgtitle: (projectDetail['custpgdetail'].map(({ pages_title }) => pages_title)),
              descp: (projectDetail['custpgdetail'].map(({ description }) => description))
            },
            termRows: {
              termid: (projectDetail['termsandcond'].map(({ terms_id }) => terms_id)),
              descp: (projectDetail['termsandcond'].map(({ description }) => description))
            }

          });
        });

    }
  }
  get formArr() {
    return this.createtask.get('termRows') as FormArray;
  }

  get formCustArr() {
    return this.createtask.get('custpgRows') as FormArray;
  }

  // get f() {
  //   return this.createtask.controls;
  // }

  initTermRows(terms?: any) {
    if (terms) {
      return this.fb.group({
        termid:terms.terms_id,
        descp: terms.description
      })
    }
    else {
      return this.fb.group({
        termid:[''],
        descp: ['']
      })
    }
  }
  initCustpgRows(cust?: any) {
    if (cust) {
      return this.fb.group({
        custid:cust.custom_testing_pages_id,
        images: cust.ss_url,
        pgtitle: cust.pages_title,
        descp: cust.description
      })

    }
    else {

      return this.fb.group({
        custid: [''],
        images: ['',Validators.required],
        pgtitle: ['',Validators.required],
        descp: ['']
      })
    }
  }

  addNewIssue(t?: any) {
    this.formArr.push(this.initTermRows(t));
  }
  addNewCustpg(t?: any) {
    this.formCustArr.push(this.initCustpgRows(t));
  }
  deleteRow(index: number,term: any) {
    if(term.termid)
    {  this.http.delete<any>(this.apiURL+'/dev/terms/delete/'+term.termid,{ headers: this.reqHeader }).subscribe(result => {
        if (result.msg)
          {
            this.formArr.removeAt(index);
          }
      })
      console.log(term.termid)
    }
    else
      {this.formArr.removeAt(index);
      }
    
  }
  deleteCustpgRow(index: number,cust: any) {
    if(cust.custid)
    {  this.http.delete<any>(this.apiURL+'/dev/custpg/delete/'+cust.custid,{ headers: this.reqHeader }).subscribe(result => {
        if (result.msg)
          {
            this.formCustArr.removeAt(index);
          }
      })
    }
    else
      {this.formCustArr.removeAt(index);
      }
  }
  goback() {
    this.location.back();
  }
  onFileSelect(event,i) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.selectedFiles.push(file);
      this.imageIndex.push(i);
    }
  }
  get formCtrl(){return this.createtask.controls;}
  onSubmit() {
    this.submitted = true;
    const formData = new FormData();
    formData.append('title', this.createtask.value.title);
    formData.append('tsb', this.createtask.value.tsb);
    formData.append('eeo', this.createtask.value.eeo);
    formData.append('iss_type', this.createtask.value.iss_type);
    formData.append('points', this.createtask.value.points);
    formData.append('age_upper', this.createtask.value.age_upper);
    formData.append('age_lower', this.createtask.value.age_lower);
    formData.append('test_type', this.createtask.value.test_type);
    formData.append('gen', this.createtask.value.gen);
    formData.append('about', this.createtask.value.about);
    formData.append('plaform', this.createtask.value.plaform);
    formData.append('type', this.createtask.value.type);
    formData.append('lengthTerms', this.createtask.value.termRows.length);
    formData.append('lengthCust', this.createtask.value.custpgRows.length);
    
    for ( let cust of this.createtask.value.custpgRows){
        formData.append('descp', cust.descp);
        formData.append('pgtitle', cust.pgtitle);
        formData.append('custid', cust.custid);
        
    }
    for ( let term of this.createtask.value.termRows){
      formData.append('termdescp', term.descp);
      formData.append('termid', term.termid);
    }
    for ( let cust of this.selectedFiles){
        formData.append('images', cust);
    }
    for ( let position of this.imageIndex){
      formData.append('imageIndex', position);
    }
    if (this.project_id == 0) {
      
      this.http.post<any>(this.apiURL+'/dev/createnewproject', formData, { headers: this.reqHeader,
        reportProgress:true,
        observe:'events'}
        ).subscribe(events => {
          if(events.type===HttpEventType.UploadProgress)
            this.progress = events.total?Math.round((events.loaded/events.total)*100):0
          if(events.type === HttpEventType.Response)
            (events.body.msg)?this.router.navigate(['/tasks-lists']):this.progress=0;
          })
          
      
    }
    else {
      this.http.post<any>(this.apiURL+'/dev/updateproject/'+this.project_id, formData, { headers: this.reqHeader,
        reportProgress:true,
        observe:'events'
       }).subscribe(events => {
          if(events.type===HttpEventType.UploadProgress)
          this.progress = events.total?Math.round((events.loaded/events.total)*100):0
          if(events.type === HttpEventType.Response)
            (events.body.msg)?this.router.navigate(['/tasks-lists']):this.progress=0;
          })
          
      
    }
  }

  testingPageType(event){
    this.formpages = (event.target.value);
  }


}
