import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyTaskCreationComponent } from './company-task-creation.component';

describe('CompanyTaskCreationComponent', () => {
  let component: CompanyTaskCreationComponent;
  let fixture: ComponentFixture<CompanyTaskCreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyTaskCreationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyTaskCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
