import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';
import { ChangePasswordProfileComponent } from './change-password-profile/change-password-profile.component';
import { CompanyTaskCreationComponent } from './company-task-creation/company-task-creation.component';
import { CompanyTaskListsCenterComponent } from './company-task-lists-center/company-task-lists-center.component';
import { CompanyTaskResponsesListsComponent } from './company-task-responses-lists/company-task-responses-lists.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { DeveloperEditProfileFormComponent } from './developer-edit-profile-form/developer-edit-profile-form.component';
import { DeveloperLoginPageComponent } from './developer-login-page/developer-login-page.component';
import { DeveloperProfilePageComponent } from './developer-profile-page/developer-profile-page.component';
import { DeveloperSignupPageComponent } from './developer-signup-page/developer-signup-page.component';
import { ExplorePageComponent } from './explore-page/explore-page.component';
import { HomePageComponent } from './home-page/home-page.component';
import { LoginSignInsPageComponent } from './login-sign-ins-page/login-sign-ins-page.component';
import { RewardsPageComponent } from './rewards-page/rewards-page.component';
import { TaskDetailsComponent } from './task-details/task-details.component';
import { TaskResponseViewComponent } from './task-response-view/task-response-view.component';
import { TesterTaskSubmissionFormComponent } from './tester-task-submission-form/tester-task-submission-form.component';
import { TestersSignupPageComponent } from './testers-signup-page/testers-signup-page.component';
import { UserEditProfileFormComponent } from './user-edit-profile-form/user-edit-profile-form.component';
import { UserTesterHistoryComponent } from './user-tester-history/user-tester-history.component';
import { UserTesterOngoingProjectListsComponent } from './user-tester-ongoing-project-lists/user-tester-ongoing-project-lists.component';
import { UserTesterProfilePageComponent } from './user-tester-profile-page/user-tester-profile-page.component';
import { TestingComponent } from './testing/testing.component';
import { LoggedOutAfterPageComponent } from './logged-out-after-page/logged-out-after-page.component';
import { GrantedResponsesListComponent } from './granted-responses-list/granted-responses-list.component';
import { GrantedResponseViewComponent } from './granted-response-view/granted-response-view.component';
import { DatePipe } from '@angular/common';

const routes: Routes = [
  {
    path: 'demotesting',
    component: TestingComponent
  },
  {
    path: 'login-signup',
    component: LoginSignInsPageComponent 
  },
  {
    path:'testers-signup-or-edit/:tester_id',
    component:TestersSignupPageComponent
  },
  {
    path:'developer-login',
    component:DeveloperLoginPageComponent
  },
  {
    path:'developer-signup-or-edit/:developer_id',
    component:DeveloperSignupPageComponent
  },
  {
    path:'home', 
    component:HomePageComponent
  },
  {
    path:'aboutus' , 
    component:AboutUsComponent
  },
  {
    path:'', redirectTo: '/home', pathMatch:'full'
  },
  {
    path:'user-tester-profile',
    component:UserTesterProfilePageComponent
  },
  {
    path:'user-tester-history',
    component:UserTesterHistoryComponent
  },
  {
    path:'explore',
    component:ExplorePageComponent
  },
  {
    path:'user-profile-edit',
    component:UserEditProfileFormComponent
  },
  {
    path:'developer-profile-edit',
    component:DeveloperEditProfileFormComponent
  },
  {
    path:'change-password',
    component:ChangePasswordProfileComponent
  },
  {
    path:'task-responses-list/:project_id',
    component:CompanyTaskResponsesListsComponent
  },
  {
    path:'tasks-lists',
    component:CompanyTaskListsCenterComponent
  },
  {
    path:'user-ongoing-task-list',
    component:UserTesterOngoingProjectListsComponent
  },
  {
    path:'project-task-details/:project_id',
    component:TaskDetailsComponent
  },
  {
    path:'task-response-view/:project_id/:enrollment_id',
    component:TaskResponseViewComponent
  },
  {
    path:'task-submission-form/:enrollment_id',
    component:TesterTaskSubmissionFormComponent
  },
  {
    path:'task-creating-or-edit-form/:project_id',
    component:CompanyTaskCreationComponent
  },
  {
    path:'profile-page',
    component:DeveloperProfilePageComponent
  },
  {
    path:'tasks-created',
    component:CompanyTaskListsCenterComponent
  },
  {
    path:'contact-us',
    component:ContactUsComponent
  },
  {
    path:'rewards',
    component:RewardsPageComponent
  },
  {
    path:'logged-out',
    component:LoggedOutAfterPageComponent
  },
  {
    path:'granted-points-list/:project_id',
    component:GrantedResponsesListComponent
  },
  {
    path:'granted-response-view/:project_id/:enrollment_id',
    component:GrantedResponseViewComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [DatePipe]
})
export class AppRoutingModule { }
