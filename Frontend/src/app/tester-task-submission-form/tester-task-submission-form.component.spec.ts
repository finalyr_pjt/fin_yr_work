import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TesterTaskSubmissionFormComponent } from './tester-task-submission-form.component';

describe('TesterTaskSubmissionFormComponent', () => {
  let component: TesterTaskSubmissionFormComponent;
  let fixture: ComponentFixture<TesterTaskSubmissionFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TesterTaskSubmissionFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TesterTaskSubmissionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
