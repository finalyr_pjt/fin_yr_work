import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders, HttpEventType } from '@angular/common/http';
import { FormGroup, FormArray, FormBuilder, Validators} from '@angular/forms';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-tester-task-submission-form',
  templateUrl: './tester-task-submission-form.component.html',
  styleUrls: ['./tester-task-submission-form.component.css']
})
export class TesterTaskSubmissionFormComponent implements OnInit {
  
  enrollment_id = '';
  selectedimage: any;
  submissionForm: any;
  pageName: any;
  selectedFiles:File[]= [];
  imageIndex:any[]= [];
  issueType: any;
  reqHeader = new HttpHeaders({ 
    'Authorization': 'Bearer ' + (localStorage.getItem('token'))
  });

  apiURL = environment.apiURL;

  addmoreissue!: FormGroup;
  user: any;
  submitted: any;
  progress: any;
  flag: boolean = false;

  constructor(
    private router: Router,
    private location: Location,
    private http: HttpClient,
    private route: ActivatedRoute,
    private fb:FormBuilder
  ) { 
}
 
  ngOnInit(): void {
    this.user = localStorage.getItem('user');
    this.route.params.subscribe(params => {
      this.enrollment_id = params['enrollment_id'];
      });
    
    this.http.get(this.apiURL+'/tester/submit_response/'+this.enrollment_id,{headers:this.reqHeader})
    .subscribe((submissionForm)=> {
      this.submissionForm=submissionForm;
      this.pageName=submissionForm["pageTitles"];
      this.issueType=submissionForm["data"].typeof;
      
    })
    
    this.addmoreissue = this.fb.group({
      issueRows: this.fb.array([this.initIssueRows()])
    });
    
    
  }
  get formArr(){
    return this.addmoreissue.get('issueRows') as FormArray;
  }

  get f(){
    return this.addmoreissue.controls;
  }
  initIssueRows(){
    return this.fb.group({
      issue_type:[''],
      cust_test_pg_id:[''],
      images:['',Validators.required],
      descp:['',Validators.required],
    })
  }

  

  addNewIssue(){ 
    this.formArr.push(this.initIssueRows());
  }
  
  deleteRow(index: number){
    this.formArr.removeAt(index);
  }

  onFileSelect(event,i) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.selectedFiles.push(file);
      this.imageIndex.push(i);
    }
  }
  onPageSelect(ss_url,i){
      this.selectedimage=ss_url;
  }

  onSubmit(){
    this.submitted = true;
    if(this.addmoreissue.valid && this.addmoreissue.touched)
    {
      const formData = new FormData();
      formData.append('lengthIssue', this.addmoreissue.value.issueRows.length);
      
      for ( let issue of this.addmoreissue.value.issueRows){
        formData.append('descp', issue.descp);
        formData.append('cust_test_pg_id', issue.cust_test_pg_id);
        if(!issue.issue_type)
          issue.issue_type=(this.issueType==='UI')?0:1;
        formData.append('issue_type', issue.issue_type);
      }
      for ( let issue_pic of this.selectedFiles){
        formData.append('images', issue_pic);
      }
      
      for ( let position of this.imageIndex){
        formData.append('imageIndex', position);
      }
      console.log("asdfsadfsda")
      this.http.post<any>(this.apiURL+'/tester/submit_response/'+this.enrollment_id, formData,{headers:this.reqHeader,
        reportProgress:true,
      observe:'events'}
      ).subscribe(events => {
      if(events.type===HttpEventType.UploadProgress)
        this.progress = events.total?Math.round((events.loaded/events.total)*100):0
      if(events.type === HttpEventType.Response)
        (events.body.msg)?this.router.navigate(['/user-ongoing-task-list']):window.location.reload();
      })
    }

  }
  goback() {
    this.location.back();
  }


}
