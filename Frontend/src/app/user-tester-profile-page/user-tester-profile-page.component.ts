import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-user-tester-profile-page',
  templateUrl: './user-tester-profile-page.component.html',
  styleUrls: ['./user-tester-profile-page.component.css']
})
export class UserTesterProfilePageComponent implements OnInit {
  reqHeader = new HttpHeaders({ 
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + (localStorage.getItem('token'))
  });

  testerDetails: any;
  ongoingDetails: any;
  submitedDetails: any;
  rewardedDetails: any;
  apiURL = environment.apiURL;
  user: any;

  constructor(
    private router: Router,
    private location: Location,
    private http: HttpClient
    ) { }

  ngOnInit(): void {
    this.user = localStorage.getItem('user');
    this.http.get(this.apiURL+'/tester/dashboard',{headers:this.reqHeader})
    .subscribe((testerDetails)=> {
      
      this.testerDetails=testerDetails;
      
    })
    this.http.get(this.apiURL+'/tester/ongoing_enrolled',{headers:this.reqHeader})
    .subscribe((ongoingDetails)=> {
      
      this.ongoingDetails=ongoingDetails;
      console.log(ongoingDetails)
      
    })
    this.http.get(this.apiURL+'/tester/submitted_enrolled',{headers:this.reqHeader})
    .subscribe((submitedDetails)=> {
      
      this.submitedDetails=submitedDetails;
      console.log(submitedDetails)
      
    })
    this.http.get(this.apiURL+'/tester/granted_enrolled',{headers:this.reqHeader})
    .subscribe((rewardedDetails)=> {
      
      this.rewardedDetails=rewardedDetails;
      console.log(rewardedDetails)
      
    })
    
  }

  goback() {
    this.location.back();
  }
}
