import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTesterProfilePageComponent } from './user-tester-profile-page.component';

describe('UserTesterProfilePageComponent', () => {
  let component: UserTesterProfilePageComponent;
  let fixture: ComponentFixture<UserTesterProfilePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserTesterProfilePageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTesterProfilePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
