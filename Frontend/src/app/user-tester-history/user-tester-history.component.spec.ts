import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTesterHistoryComponent } from './user-tester-history.component';

describe('UserTesterHistoryComponent', () => {
  let component: UserTesterHistoryComponent;
  let fixture: ComponentFixture<UserTesterHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserTesterHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTesterHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
