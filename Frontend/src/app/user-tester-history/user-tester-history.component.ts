import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-user-tester-history',
  templateUrl: './user-tester-history.component.html',
  styleUrls: ['./user-tester-history.component.css']
})
export class UserTesterHistoryComponent implements OnInit {
  user: any;

  constructor(private location: Location,private http: HttpClient) { }
  inReviewProjects:any;
  grantedProjects:any;
  apiURL = environment.apiURL;
  reqHeader = new HttpHeaders({ 
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + (localStorage.getItem('token'))
  });

  ngOnInit(): void {
    this.user = localStorage.getItem('user');
    this.http.get(this.apiURL+'/tester/submitted_enrolled',{headers:this.reqHeader})
    .subscribe((inReviewProjects)=> {
      this.inReviewProjects=inReviewProjects;
    });
    this.http.get(this.apiURL+'/tester/granted_enrolled',{headers:this.reqHeader})
    .subscribe((grantedProjects)=> {
      this.grantedProjects=grantedProjects;
    });
  }

  activeTab : any = 'completedproject';

  tabswitch(activeTab){
    this.activeTab = activeTab;
  }

  goback() {
    this.location.back();
  }

}
