import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyTaskListsCenterComponent } from './company-task-lists-center.component';

describe('CompanyTaskListsCenterComponent', () => {
  let component: CompanyTaskListsCenterComponent;
  let fixture: ComponentFixture<CompanyTaskListsCenterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyTaskListsCenterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyTaskListsCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
