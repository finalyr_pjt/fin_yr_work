import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import AOS from 'aos';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-company-task-lists-center',
  templateUrl: './company-task-lists-center.component.html',
  styleUrls: ['./company-task-lists-center.component.css']
})
export class CompanyTaskListsCenterComponent implements OnInit {
  taskLists: any;
  reqHeader = new HttpHeaders({ 
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + (localStorage.getItem('token'))
  });
  apiURL = environment.apiURL;
  user:any;
  constructor(
    private router: Router,
    private location: Location,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.user = localStorage.getItem('user');
    this.http.get(this.apiURL+'/dev/myprojects',{headers:this.reqHeader})
    .subscribe((taskLists)=> {
      this.taskLists=taskLists;
    })

    AOS.init();
  }

  goback() {
    this.location.back();
  }

  newtask(){
    this.router.navigate(['/task-creating-or-edit-form/0']);
  }

  responsestask(project_id){
    this.router.navigate(['/task-responses-list/'+project_id]);
  }

  deactivetask(project_id){
    this.http.get(this.apiURL+'/dev/deactive_project/'+project_id,{headers:this.reqHeader})
    .subscribe((result)=> {
      if(result['msg']){
        window.location.reload();
      }
    })
  }
  activetask(project_id){
    this.http.get(this.apiURL+'/dev/active_project/'+project_id,{headers:this.reqHeader})
    .subscribe((result)=> {
      if(result['msg']){
        window.location.reload();
      }
    })
  }
  
  detailtask(project_id){
    this.router.navigate(['/project-task-details/'+project_id]);
  }

  editTask(project_id){
    this.router.navigate(['/task-creating-or-edit-form/'+project_id]);
  }

}
