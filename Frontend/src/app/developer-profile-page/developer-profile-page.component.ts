import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-developer-profile-page',
  templateUrl: './developer-profile-page.component.html',
  styleUrls: ['./developer-profile-page.component.css']
})
export class DeveloperProfilePageComponent implements OnInit {

  reqHeader = new HttpHeaders({ 
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + (localStorage.getItem('token'))
  });

  developerDetails: any;
  apiURL = environment.apiURL;
  user:any;
  constructor(
    private router: Router,
    private location: Location,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.user = localStorage.getItem('user');
    this.http.get(this.apiURL+'/dev/dashboard',{headers:this.reqHeader})
    .subscribe((developerDetails)=> {
      
      this.developerDetails=developerDetails;
      
    })
  }
 
  goback() {
    this.location.back();
  }
  
}
