import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { environment } from 'src/environments/environment.prod';
import { HttpClient, HttpEventType, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {NG_VALIDATORS} from '@angular/forms';

@Component({
  selector: 'app-testers-signup-page',
  templateUrl: './testers-signup-page.component.html',
  styleUrls: ['./testers-signup-page.component.css']
})
export class TestersSignupPageComponent implements OnInit {

  apiURL = environment.apiURL;
  createtester!: FormGroup;
  tester_id: any;
  selectedFile: any;
  reqHeader = new HttpHeaders({
    'Authorization': 'Bearer ' + (localStorage.getItem('token'))
  });
  submitted: any;
  progress: any;

  constructor(
    private router: Router,
    private location: Location,
    private http: HttpClient,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.createtester = this.fb.group({
      testername:['',Validators.required],
      email:['',Validators.required],
      password2:['',Validators.required],
      gender:['',Validators.required],
      dob:['',Validators.required],
      password:['',Validators.required],
      profilepic:['']
    })
    this.route.params.subscribe(params => {
      this.tester_id = params['tester_id'];
    });
    if(this.tester_id)
      this.getTester(this.tester_id);
  }

  private getTester(tester_id: number){
    if(tester_id==0)
    {}
    else
        {
          this.http.get(this.apiURL+'/tester/dashboard', { headers: this.reqHeader })
          .subscribe((testerDetail) => {
            const gen = testerDetail['gender']=='Male'?0:1;
            this.createtester.patchValue({
              testername: testerDetail['name'],
              email: testerDetail['email'],
              gender: gen,
              dob: testerDetail['date_of_birth'],
              profilepic: testerDetail['profile_pic']
            })
          
          })
        }
  }
  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.selectedFile = file;
    }
  }
  get formCtrl(){return this.createtester.controls;}
  onSubmit() {
    this.submitted = true;
    const formData = new FormData();
    formData.append('testername', this.createtester.value.testername);
    formData.append('email', this.createtester.value.email);
    formData.append('password', this.createtester.value.password);
    formData.append('password2', this.createtester.value.password2);
    formData.append('gender', this.createtester.value.gender);
    formData.append('dob', this.createtester.value.dob);
    formData.append('profilepic', this.selectedFile);
    if (this.tester_id == 0) {
      this.http.post<any>(this.apiURL+'/tester/createnew',formData,
      {reportProgress:true,
      observe:'events'}).subscribe(events => {
        if(events.type===HttpEventType.UploadProgress)
        this.progress = events.total?Math.round((events.loaded/events.total)*100):0
        if(events.type === HttpEventType.Response){
        localStorage.setItem('token',events.body.token);
        localStorage.setItem('user','tester');
        if(events.body.token)
          this.router.navigate(['/user-ongoing-task-list'])
        }
      })
    }
    else {
      const formData = new FormData();
      formData.append('testername', this.createtester.value.testername);
      formData.append('email', this.createtester.value.email);
      formData.append('gender', this.createtester.value.gender);
      formData.append('dob', this.createtester.value.dob);
      formData.append('profilepic', this.selectedFile);
      this.http.post<any>(this.apiURL+'/tester/edit/'+this.tester_id, formData,{headers:this.reqHeader,
        reportProgress:true,
        observe:'events'}).subscribe(events => {
          if(events.type===HttpEventType.UploadProgress)
          this.progress = events.total?Math.round((events.loaded/events.total)*100):0
          if(events.type === HttpEventType.Response)
            (events.body.msg)?this.router.navigate(['/user-tester-profile']):window.location.reload();
          
      })
    }
  }



}
