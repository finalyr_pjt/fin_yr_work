import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestersSignupPageComponent } from './testers-signup-page.component';

describe('TestersSignupPageComponent', () => {
  let component: TestersSignupPageComponent;
  let fixture: ComponentFixture<TestersSignupPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestersSignupPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestersSignupPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
