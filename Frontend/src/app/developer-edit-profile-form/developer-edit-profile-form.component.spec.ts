import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeveloperEditProfileFormComponent } from './developer-edit-profile-form.component';

describe('DeveloperEditProfileFormComponent', () => {
  let component: DeveloperEditProfileFormComponent;
  let fixture: ComponentFixture<DeveloperEditProfileFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeveloperEditProfileFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeveloperEditProfileFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
