import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-developer-edit-profile-form',
  templateUrl: './developer-edit-profile-form.component.html',
  styleUrls: ['./developer-edit-profile-form.component.css']
})
export class DeveloperEditProfileFormComponent implements OnInit {

  constructor(
    private router: Router,
    private location: Location
  ) { }

  ngOnInit(): void {
  }

  goback() {
    this.location.back();
  }

}
