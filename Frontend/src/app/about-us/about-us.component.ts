import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import AOS from 'aos';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {

  constructor(
    private location: Location,
    private router:Router
  ) { }

  ngOnInit(): void {
    AOS.init();
  }
  
  goback() {
    this.location.back();
  }

  contact(){
    this.router.navigate(['/contact-us'])
  }

  developerlogin(){
    this.router.navigate(['/developer-login']);
  }
}
