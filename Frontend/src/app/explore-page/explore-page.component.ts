import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import AOS from 'aos';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-explore-page',
  templateUrl: './explore-page.component.html',
  styleUrls: ['./explore-page.component.css']
})
export class ExplorePageComponent implements OnInit {
  androidProjects: any;
  websiteProjects: any;
  iosProjects: any;
  uiProjects: any;
  functionalityProjects: any;
  project_id: any;
  apiURL = environment.apiURL;
  constructor(
    private router: Router,
    private location: Location,
    private http: HttpClient
    ) {
   }
   reqHeader = new HttpHeaders({ 
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + (localStorage.getItem('token'))
  });
  ngOnInit(): void {
    this.http.get(this.apiURL+'/projects/android')
    .subscribe((androidProjects)=> {
      this.androidProjects=androidProjects;
    })
    this.http.get(this.apiURL+'/projects/website')
    .subscribe((websiteProjects)=> {
      this.websiteProjects=websiteProjects;
    })
    this.http.get(this.apiURL+'/projects/ios')
    .subscribe((iosProjects)=> {
      this.iosProjects=iosProjects;
    })
    this.http.get(this.apiURL+'/projects/ui')
    .subscribe((uiProjects)=> {
      this.uiProjects=uiProjects;
    })
    this.http.get(this.apiURL+'/projects/functionality')
    .subscribe((functionalityProjects)=> {
      this.functionalityProjects=functionalityProjects;
    })

    AOS.init();
    
  }
  
  activeTab : any = 'Android';
  
  tabswitch(activeTab){
    this.activeTab = activeTab;
  }

  goback(){
    this.location.back();
  }
  
  enroll(project_id){
    if(localStorage.getItem('user')==='tester')
    {
      this.http.get(this.apiURL+'/tester/enroll/'+project_id,{headers:this.reqHeader}).subscribe((result)=> {
        if(result['msg']){
          this.router.navigate(['/user-ongoing-task-list'])
        }
      })
    }
    else{
      this.router.navigate(['/login-signup'])
    }
  }

  detailtask(project_id){
    this.router.navigate(['/project-task-details/'+project_id]);
  }

}
