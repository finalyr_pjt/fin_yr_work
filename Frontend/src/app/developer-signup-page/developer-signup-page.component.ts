import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { environment } from 'src/environments/environment.prod';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpEventType, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-developer-signup-page',
  templateUrl: './developer-signup-page.component.html',
  styleUrls: ['./developer-signup-page.component.css']
})
export class DeveloperSignupPageComponent implements OnInit {

  apiURL = environment.apiURL;
  createdeveloper!: FormGroup;
  developer_id: any;
  selectedFile: any;
  reqHeader = new HttpHeaders({
    'Authorization': 'Bearer ' + (localStorage.getItem('token'))
  });
  submitted: any;
  progress: any;

  constructor(
    private router: Router,
    private location: Location,
    private http: HttpClient,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.createdeveloper = this.fb.group({
      developername:['',Validators.required],
      email:['',Validators.required],
      password2:['',Validators.required],
      type:[''],
      aboutus:['',Validators.required],
      password:['',Validators.required],
      profilepic:['',Validators.required]
    })
    this.route.params.subscribe(params => {
      this.developer_id = params['developer_id'];
    });
    if(this.developer_id)
      this.getDeveloper(this.developer_id);
  }

  private getDeveloper(developer_id:number){
    if(developer_id==0){

    }
    else{
      this.http.get(this.apiURL+'/dev/dashboard', { headers: this.reqHeader })
      .subscribe((devDetail) => {
          this.createdeveloper.patchValue({
            developername: devDetail[0]['name'],
            email: devDetail[0]['email'],
            type: devDetail[0]['developer_type'],
            aboutus: devDetail[0]['about_us'],
            profilepic: devDetail[0]['pic_url']
          })
      })
    }
  }
  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.selectedFile = file;
    }
  }
  get formCtrl(){return this.createdeveloper.controls;}
  onSubmit() {
    this.submitted = true;
    const formData = new FormData();
    formData.append('devname', this.createdeveloper.value.developername);
    formData.append('email', this.createdeveloper.value.email);
    formData.append('password', this.createdeveloper.value.password);
    formData.append('password2', this.createdeveloper.value.password2);
    formData.append('type', this.createdeveloper.value.type);
    formData.append('aboutus', this.createdeveloper.value.aboutus);
    formData.append('profilepic', this.selectedFile);
    if (this.developer_id == 0) {
      this.http.post<any>(this.apiURL+'/dev/createnew',formData,
      {reportProgress:true,
    observe:'events'}).subscribe(events => {
      if(events.type===HttpEventType.UploadProgress)
        this.progress = events.total?Math.round((events.loaded/events.total)*100):0
      if(events.type === HttpEventType.Response){
        localStorage.setItem('token',events.body.token);
        localStorage.setItem('user','developer');
        if(events.body.token)
          this.router.navigate(['/tasks-lists'])
      }
      })
    }
    else {
      const formData = new FormData();
      formData.append('devname', this.createdeveloper.value.developername);
      formData.append('email', this.createdeveloper.value.email);
      formData.append('type', this.createdeveloper.value.type);
      formData.append('aboutus', this.createdeveloper.value.aboutus);
      formData.append('profilepic', this.selectedFile);
      this.http.post<any>(this.apiURL+'/dev/edit/'+this.developer_id, formData,{headers:this.reqHeader,
        reportProgress:true,
        observe:'events'
      }).subscribe(events => {
        if(events.type===HttpEventType.UploadProgress)
        this.progress = events.total?Math.round((events.loaded/events.total)*100):0
        if(events.type === HttpEventType.Response)
          (events.body.msg)?this.router.navigate(['/profile-page']):window.location.reload();
      })
    }
  }
}
