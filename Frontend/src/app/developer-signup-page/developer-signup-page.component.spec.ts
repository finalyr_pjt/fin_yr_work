import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeveloperSignupPageComponent } from './developer-signup-page.component';

describe('DeveloperSignupPageComponent', () => {
  let component: DeveloperSignupPageComponent;
  let fixture: ComponentFixture<DeveloperSignupPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeveloperSignupPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeveloperSignupPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
