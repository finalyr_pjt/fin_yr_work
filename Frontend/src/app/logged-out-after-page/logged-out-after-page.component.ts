import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-logged-out-after-page',
  templateUrl: './logged-out-after-page.component.html',
  styleUrls: ['./logged-out-after-page.component.css']
})
export class LoggedOutAfterPageComponent implements OnInit {

  constructor(private router:Router,private http: HttpClient) { }

  ngOnInit(): void {
  }

  navigatetohome(){
    this.router.navigate(['/home']);
  }
  
    redirect_Page (){
        let tID = setTimeout(function () {
            window.location.href = "./home";
            window.clearTimeout(tID);		
        }, 200);
    }
    

}
