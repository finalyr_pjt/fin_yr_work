import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoggedOutAfterPageComponent } from './logged-out-after-page.component';

describe('LoggedOutAfterPageComponent', () => {
  let component: LoggedOutAfterPageComponent;
  let fixture: ComponentFixture<LoggedOutAfterPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoggedOutAfterPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggedOutAfterPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
