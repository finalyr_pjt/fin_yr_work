import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import AOS from 'aos';

@Component({
  selector: 'app-granted-response-view',
  templateUrl: './granted-response-view.component.html',
  styleUrls: ['./granted-response-view.component.css']
})
export class GrantedResponseViewComponent implements OnInit {
  project_id: any;

  reqHeader = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + (localStorage.getItem('token'))
  });
  issuesReported: any;
  enrollment_id: any;
  apiURL = environment.apiURL;
  user: any;
  constructor(
    private router: Router,
    private location: Location,
    private http: HttpClient,
    private route: ActivatedRoute
  ) { }

  activeui : Boolean = true;
  activefunctn : Boolean = true;


  ngOnInit(): void {
    this.user = localStorage.getItem('user');
    this.route.params.subscribe(params => {
      this.project_id = params['project_id'];
      this.enrollment_id = params['enrollment_id'];
    });
    this.http.get(this.apiURL+'/dev/granted/'+this.project_id+'/'+this.enrollment_id,{headers:this.reqHeader})
    .subscribe((issuesReported)=> {
      this.issuesReported=issuesReported;
      console.log(issuesReported);
    })

    AOS.init();
    
  }

  goback() {
    this.location.back();
  }

 
  contactTester(email_id){
    // window.open('mailto:'+email_id+'&subject=&body=', '_self'); 
    location.href = "mailto:"+email_id;
  }

  uiissuebtn(){
    this.activeui= !this.activeui;
  }

  fnissuebtn(){
    this.activefunctn= !this.activefunctn;
  }

  clearall(){
    this.activefunctn= true;
    this.activeui= true;
  }

  
}
