import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GrantedResponseViewComponent } from './granted-response-view.component';

describe('GrantedResponseViewComponent', () => {
  let component: GrantedResponseViewComponent;
  let fixture: ComponentFixture<GrantedResponseViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GrantedResponseViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GrantedResponseViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
