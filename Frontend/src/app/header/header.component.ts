import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  reqHeader = new HttpHeaders({ 
    'Authorization': 'Bearer ' + (localStorage.getItem('token'))
  });

  apiURL = environment.apiURL;
  user:any;
  constructor(private router: Router,
    private http: HttpClient
    ) { }

  logoutTester(){
    this.http.get(this.apiURL+'/tester/logout',{headers:this.reqHeader})
    .subscribe((result)=> {
      localStorage.setItem('token',result['token']);
      if(result['token']=='')
      {
        localStorage.setItem('token',result['token']);
        localStorage.setItem('user','');
        this.router.navigate(['/login-signup'])
      }
    })
  }
  logoutDev(){
    this.http.get(this.apiURL+'/dev/logout',{headers:this.reqHeader})
    .subscribe((result)=> {
      localStorage.setItem('token',result['token']);
      if(result['token']=='')
      {
        localStorage.setItem('token',result['token']);
        localStorage.setItem('user','');
        this.router.navigate(['/developer-login'])
      }
    })
  }
  ngOnInit(): void {
    this.user = localStorage.getItem('user');
  }

  navigatetopage() {
    this.router.navigate(['/login-signup']);
  }
}
