import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header-non-log',
  templateUrl: './header-non-log.component.html',
  styleUrls: ['./header-non-log.component.css']
})
export class HeaderNonLogComponent implements OnInit {

  constructor(
    private router: Router
    ) { }

  ngOnInit(): void {
    
  }
  navigatetopage() {
    this.router.navigate(['/login-signup']);
  }

}
