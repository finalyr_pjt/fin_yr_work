import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderNonLogComponent } from './header-non-log.component';

describe('HeaderNonLogComponent', () => {
  let component: HeaderNonLogComponent;
  let fixture: ComponentFixture<HeaderNonLogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderNonLogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderNonLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
