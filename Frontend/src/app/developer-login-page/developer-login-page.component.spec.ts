import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeveloperLoginPageComponent } from './developer-login-page.component';

describe('DeveloperLoginPageComponent', () => {
  let component: DeveloperLoginPageComponent;
  let fixture: ComponentFixture<DeveloperLoginPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeveloperLoginPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeveloperLoginPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
