import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import AOS from 'aos';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-developer-login-page',
  templateUrl: './developer-login-page.component.html',
  styleUrls: ['./developer-login-page.component.css']
})
export class DeveloperLoginPageComponent implements OnInit {
  submitted: any;

  constructor(
    private router: Router,
    private location: Location,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.submitted=false;
    AOS.init();
  }
  apiURL = environment.apiURL;
  userlogged(data){
    // console.warn(data.value.email,data.value.password)
    this.submitted=true;
    this.http.post<any>(this.apiURL+'/dev/login',  {email:data.value.email,password:data.value.password}).subscribe(result => {
    if(result.token)
      {
        localStorage.setItem('token',result['token']);
        localStorage.setItem('user','developer');
        this.router.navigate(['/tasks-lists'])
      }
    })
  }

}
