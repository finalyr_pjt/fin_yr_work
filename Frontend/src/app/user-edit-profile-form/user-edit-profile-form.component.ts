import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-user-edit-profile-form',
  templateUrl: './user-edit-profile-form.component.html',
  styleUrls: ['./user-edit-profile-form.component.css']
})
export class UserEditProfileFormComponent implements OnInit {

  constructor(
    private router: Router,
    private location: Location
  ) { }

  ngOnInit(): void {
  }

  goback() {
    this.location.back();
  }
}
