import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css']
})
export class TaskDetailsComponent implements OnInit {
  apiURL = environment.apiURL;
  project_id = '';
  projectDetail: any;
  devDetail: any;
  message:any;
  reqHeader = new HttpHeaders({ 
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + (localStorage.getItem('token'))
  });
  user: any;
  constructor(
    private router: Router,
    private location: Location,
    private route: ActivatedRoute,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.user = localStorage.getItem('user')
    this.route.params.subscribe(params => {
      this.project_id = params['project_id'];
      });
      if(this.user==='developer'){
        this.http.get(this.apiURL+'/dev/project/'+this.project_id,{headers:this.reqHeader})
        .subscribe((projectdevDetail)=> {
          if(projectdevDetail['proj_detail'])
            this.projectDetail=projectdevDetail;
          else{
            this.http.get(this.apiURL+'/project/'+this.project_id)
            .subscribe((projectDetail)=> {
              if(projectDetail['proj_detail'])
              this.projectDetail=projectDetail;
            })
          }
        })
      }
      else{
        this.http.get(this.apiURL+'/project/'+this.project_id)
        .subscribe((projectDetail)=> {
          if(projectDetail['proj_detail'])
          this.projectDetail=projectDetail;
        })
      }
      if(this.user==='tester'){
        this.http.get(this.apiURL+'/tester/eligibleproject/'+this.project_id,{headers:this.reqHeader}).subscribe((result)=> {
          if(result['msg']){
            this.message='eligible';
          }
          if(result['gender']){
            this.message='gender'
          }
          if(result['agelimit']){
            this.message='agelimit'
          }
          if(result['both']){
            this.message='both'
          }
          if(result['alreadyenrolled']){
            this.message='already'
            if(result['proj_detail'])
              this.projectDetail=result;
          }
          console.log(result,this.message);
        })
      }
  }
  
  enroll(){
    if(this.user=='tester'){
      this.http.get(this.apiURL+'/tester/enroll/'+this.project_id,{headers:this.reqHeader}).subscribe((result)=> {
        if(result['msg']){
          this.router.navigate(['/user-ongoing-task-list'])
        }
      })
    }
    if(this.user==''){
      this.router.navigate(['/login-signup'])
    }
  }

  goback() {
    this.location.back();
  }
}
