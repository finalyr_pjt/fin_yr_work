import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderNonLogComponent } from './header-non-log/header-non-log.component';
import { HomePageComponent } from './home-page/home-page.component';
import { FooterComponent } from './footer/footer.component';
import { LoginSignInsPageComponent } from './login-sign-ins-page/login-sign-ins-page.component';
import { UserTesterProfilePageComponent } from './user-tester-profile-page/user-tester-profile-page.component';
import { UserTesterHistoryComponent } from './user-tester-history/user-tester-history.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ExplorePageComponent } from './explore-page/explore-page.component';
import { HeaderComponent } from './header/header.component';
import { CompanyHeaderLoggedInComponent } from './company-header-logged-in/company-header-logged-in.component';
import { CompanyTaskResponsesListsComponent } from './company-task-responses-lists/company-task-responses-lists.component';
import { CompanyTaskListsCenterComponent } from './company-task-lists-center/company-task-lists-center.component';
import { UserTesterOngoingProjectListsComponent } from './user-tester-ongoing-project-lists/user-tester-ongoing-project-lists.component';
import { TaskDetailsComponent } from './task-details/task-details.component';
import { TaskResponseViewComponent } from './task-response-view/task-response-view.component';
import { TesterTaskSubmissionFormComponent } from './tester-task-submission-form/tester-task-submission-form.component';
import { CompanyTaskCreationComponent } from './company-task-creation/company-task-creation.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { TestersSignupPageComponent } from './testers-signup-page/testers-signup-page.component';
import { DeveloperLoginPageComponent } from './developer-login-page/developer-login-page.component';
import { DeveloperSignupPageComponent } from './developer-signup-page/developer-signup-page.component';
import { RewardsPageComponent } from './rewards-page/rewards-page.component';
import { DeveloperProfilePageComponent } from './developer-profile-page/developer-profile-page.component';
import { UserEditProfileFormComponent } from './user-edit-profile-form/user-edit-profile-form.component';
import { DeveloperEditProfileFormComponent } from './developer-edit-profile-form/developer-edit-profile-form.component';
import { ChangePasswordProfileComponent } from './change-password-profile/change-password-profile.component';
import { TestingComponent } from './testing/testing.component';
import { LoggedOutAfterPageComponent } from './logged-out-after-page/logged-out-after-page.component';
import { GrantedResponsesListComponent } from './granted-responses-list/granted-responses-list.component';
import { GrantedResponseViewComponent } from './granted-response-view/granted-response-view.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderNonLogComponent,
    HomePageComponent,
    FooterComponent,
    LoginSignInsPageComponent,
    UserTesterProfilePageComponent,
    UserTesterHistoryComponent,
    AboutUsComponent,
    ExplorePageComponent,
    HeaderComponent,
    CompanyHeaderLoggedInComponent,
    CompanyTaskResponsesListsComponent,
    CompanyTaskListsCenterComponent,
    UserTesterOngoingProjectListsComponent,
    TaskDetailsComponent,
    TaskResponseViewComponent,
    TesterTaskSubmissionFormComponent,
    CompanyTaskCreationComponent,
    ContactUsComponent,
    TestersSignupPageComponent,
    DeveloperLoginPageComponent,
    DeveloperSignupPageComponent,
    RewardsPageComponent,
    DeveloperProfilePageComponent,
    UserEditProfileFormComponent,
    DeveloperEditProfileFormComponent,
    ChangePasswordProfileComponent,
    TestingComponent,
    LoggedOutAfterPageComponent,
    GrantedResponsesListComponent,
    GrantedResponseViewComponent,
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [AppComponent,],
  bootstrap: [AppComponent]
})
export class AppModule { }
