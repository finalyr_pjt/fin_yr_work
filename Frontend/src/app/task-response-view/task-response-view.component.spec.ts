import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskResponseViewComponent } from './task-response-view.component';

describe('TaskResponseViewComponent', () => {
  let component: TaskResponseViewComponent;
  let fixture: ComponentFixture<TaskResponseViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaskResponseViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskResponseViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
