import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import AOS from 'aos';
import { environment } from 'src/environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-rewards-page',
  templateUrl: './rewards-page.component.html',
  styleUrls: ['./rewards-page.component.css']
})
export class RewardsPageComponent implements OnInit {
  apiURL = environment.apiURL;
  fashionProjects: any;
  onlineProjects: any;
  foodProjects: any;
  giftProjects: any;
  reward_id: any;
  constructor(
    private router: Router,
    private location: Location,
    private http: HttpClient
  ) { }

  reqHeader = new HttpHeaders({ 
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + (localStorage.getItem('token'))
  });  
  
  ngOnInit(): void {
    this.http.get(this.apiURL+'/rewards/fashion')
    .subscribe((fashionProjects)=> {
      this.fashionProjects=fashionProjects;
    })
    this.http.get(this.apiURL+'/rewards/online-retail')
    .subscribe((onlineProjects)=> {
      this.onlineProjects=onlineProjects;
    })
    this.http.get(this.apiURL+'/rewards/food')
    .subscribe((foodProjects)=> {
      this.foodProjects=foodProjects;
    })
    this.http.get(this.apiURL+'/rewards/gift-coupon')
    .subscribe((giftProjects)=> {
      this.giftProjects=giftProjects;
    })
    AOS.init();
  }

  activeTab : any = 'Fashion';
  

  tabswitch(activeTab){
    this.activeTab = activeTab;
  }

  goback(){
    this.location.back();
  }

  assign(reward_id){
    this.reward_id = reward_id;
  }

  redeem(reward_id?){
    if(localStorage.getItem('user')=='tester'){
      this.http.get(this.apiURL+'/tester/redeem/'+this.reward_id,{headers:this.reqHeader})
        .subscribe((result)=> {
          if(result['msg'])
            this.router.navigate(['/']);
        })
    }
    else{
      this.router.navigate(['/login-signup'])
    }
  }

}
