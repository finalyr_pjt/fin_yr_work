import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import AOS from 'aos';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-login-sign-ins-page',
  templateUrl: './login-sign-ins-page.component.html',
  styleUrls: ['./login-sign-ins-page.component.css']
})
export class LoginSignInsPageComponent implements OnInit {
  
  apiURL = environment.apiURL;
  submitted:any;
  modal:any;

  constructor(
    private router: Router,
    private location: Location,
    private http: HttpClient
  ) { }
  ngOnInit(): void {
    this.submitted=false;
    AOS.init();
  }

  goback() {
    this.location.back();
  }

  
  

  userlogged(data){
    // console.warn(data.value.email,data.value.password)
      this.submitted=true;
      this.http.post<any>(this.apiURL+'/tester/login',  {email:data.value.email,password:data.value.password}).subscribe(result => {
        
        if(result.token)
          {
            localStorage.setItem('token',result.token);
            localStorage.setItem('user','tester');
            
            this.router.navigate(['/user-ongoing-task-list'])
          }

        })
  }
  
   

}
