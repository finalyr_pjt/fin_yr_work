import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginSignInsPageComponent } from './login-sign-ins-page.component';

describe('LoginSignInsPageComponent', () => {
  let component: LoginSignInsPageComponent;
  let fixture: ComponentFixture<LoginSignInsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginSignInsPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginSignInsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
