import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  constructor(
    private router: Router,
    private location: Location,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
  }
  apiURL = environment.apiURL;
  
  contactUs(details){
    this.http.post<any>(this.apiURL+'/contactUs',  {name:details.value.name,email:details.value.emailid,phoneno:details.value.phoneno,message:details.value.msg})
    .subscribe(result => {
      if(result.msg){
        this.router.navigate(['/'])
      }
    })
  }

  goback() {
    this.location.back();
  }

}
