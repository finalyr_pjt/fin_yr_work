import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTesterOngoingProjectListsComponent } from './user-tester-ongoing-project-lists.component';

describe('UserTesterOngoingProjectListsComponent', () => {
  let component: UserTesterOngoingProjectListsComponent;
  let fixture: ComponentFixture<UserTesterOngoingProjectListsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserTesterOngoingProjectListsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTesterOngoingProjectListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
