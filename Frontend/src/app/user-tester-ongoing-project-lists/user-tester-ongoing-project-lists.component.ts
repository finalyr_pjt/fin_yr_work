import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import AOS from 'aos';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-user-tester-ongoing-project-lists',
  templateUrl: './user-tester-ongoing-project-lists.component.html',
  styleUrls: ['./user-tester-ongoing-project-lists.component.css']
})
export class UserTesterOngoingProjectListsComponent implements OnInit {
  onGoingProjects: any;
  reqHeader = new HttpHeaders({ 
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + (localStorage.getItem('token'))
  });
  apiURL = environment.apiURL;
  user: any;
  constructor(
    private router: Router,
    private location: Location,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.user = localStorage.getItem('user');
    this.http.get(this.apiURL+'/tester/ongoing_enrolled',{headers:this.reqHeader})
    .subscribe((onGoingProjects)=> {
      this.onGoingProjects=onGoingProjects;
      
    })

    AOS.init();
    
  }

  unenrollbtn(enrollment_id){
    
    this.http.get(this.apiURL+'/tester/un_enroll/'+enrollment_id,{headers:this.reqHeader})
    .subscribe((result)=> {
      if(result["msg"])
        window.location.reload();
      // if(result["error"])
      //   pop error
    })
  }
  goback() {
    this.location.back();
  }

  historybtn(){
    this.router.navigate(['/user-tester-history'])
  }

  taskdetailsbtn(project_id){
    this.router.navigate(['/project-task-details/'+project_id])
  }

  submitresponsesbtn(enrollment_id){
    this.router.navigate(['/task-submission-form/'+enrollment_id])
  }

}
