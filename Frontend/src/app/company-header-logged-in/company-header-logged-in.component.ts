import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment.prod';
@Component({
  selector: 'app-company-header-logged-in',
  templateUrl: './company-header-logged-in.component.html',
  styleUrls: ['./company-header-logged-in.component.css']
})
export class CompanyHeaderLoggedInComponent implements OnInit {
  reqHeader = new HttpHeaders({ 
    'Authorization': 'Bearer ' + (localStorage.getItem('token'))
  });

  apiURL = environment.apiURL;
  
  constructor(private router:Router,
    private http: HttpClient) { }
  
  logout(){
    this.http.get(this.apiURL+'/dev/logout',{headers:this.reqHeader})
    .subscribe((result)=> {
      localStorage.setItem('token',result['token']);
      if(result['token']=='')
      {
        localStorage.setItem('token',result['token']);
        localStorage.setItem('user','');
        this.router.navigate(['/developer-login'])
      }
    })
  }
    
  ngOnInit(): void {
  }

}
