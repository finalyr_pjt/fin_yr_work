import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyHeaderLoggedInComponent } from './company-header-logged-in.component';

describe('CompanyHeaderLoggedInComponent', () => {
  let component: CompanyHeaderLoggedInComponent;
  let fixture: ComponentFixture<CompanyHeaderLoggedInComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyHeaderLoggedInComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyHeaderLoggedInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
