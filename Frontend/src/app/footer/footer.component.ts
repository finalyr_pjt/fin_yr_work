import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  iflogged :any;
  reqHeader = new HttpHeaders({ 
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + (localStorage.getItem('token'))
  });
  apiURL = environment.apiURL;

  constructor(private router: Router,
    private http: HttpClient) { }

  deactivateProfile(type){
    if(type == 'developer'){
      this.http.get(this.apiURL+'/dev/deactivate',{headers:this.reqHeader}).subscribe((result)=> {
        if(result['msg']){
          localStorage.setItem('token','');
          localStorage.setItem('user','');
          this.router.navigate(['/'])
        }
      })
    }
    if(type=='tester'){
      this.http.get(this.apiURL+'/tester/deactivate',{headers:this.reqHeader}).subscribe((result)=> {
        if(result['msg']){
          localStorage.setItem('token','');
          localStorage.setItem('user','');
          this.router.navigate(['/'])
        }
      })
    }
  }
  ngOnInit(): void {
    this.iflogged=localStorage.getItem('user')
  }

}
