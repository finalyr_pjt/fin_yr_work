import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-change-password-profile',
  templateUrl: './change-password-profile.component.html',
  styleUrls: ['./change-password-profile.component.css']
})
export class ChangePasswordProfileComponent implements OnInit {

  reqHeader = new HttpHeaders({ 
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + (localStorage.getItem('token'))
  });
  user: any;
  apiURL = environment.apiURL;
  email: any;
  testerDetails: any;
  submitted: any;
  submittedemail: any;
  constructor(
    private router: Router,
    private location: Location,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    
    this.email = ''
    this.user = localStorage.getItem('user');
    if(this.user==='tester'){
      this.http.get(this.apiURL+'/tester/dashboard',{headers:this.reqHeader})
        .subscribe((testerDetails)=> {
          
          this.email=testerDetails['email'];
          this.http.post<any>(this.apiURL+'/forgot-or-reset-password',  {email:this.email}).subscribe(result => {
            if(result.email)
              {
                console.log(result);
              }
          }) 
        })
        
        
    }
    if(this.user==='developer'){
      this.http.get(this.apiURL+'/dev/dashboard', { headers: this.reqHeader })
        .subscribe((developerDetail) => { 
          this.email=developerDetail['email'];
          this.http.post<any>(this.apiURL+'/forgot-or-reset-password',  {email:this.email}).subscribe(result => {
            if(result.email)
              {
                console.log(result);
              }
          })
        })
        
    }

  }
  enterEmail(data){
    this.submittedemail=true
    this.email=data.value.email
    this.http.post<any>(this.apiURL+'/forgot-or-reset-password',  {email:this.email}).subscribe(result => {
      if(result.email)
        {
          console.log(result);
        }
    })
  }
  enterNewPassword(data){
    this.submitted=true
    this.http.post<any>(this.apiURL+'/verify-otp-and-password-reset',  {userotp:data.value.otp,password:data.value.password,password2:data.value.password2,email:this.email}).subscribe(result => {
      if(result.msg)
        {
          this.router.navigate(['/']);
        }
    })
  }
  goback() {
    this.location.back();
  }

}
