import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyTaskResponsesListsComponent } from './company-task-responses-lists.component';

describe('CompanyTaskResponsesListsComponent', () => {
  let component: CompanyTaskResponsesListsComponent;
  let fixture: ComponentFixture<CompanyTaskResponsesListsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyTaskResponsesListsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyTaskResponsesListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
