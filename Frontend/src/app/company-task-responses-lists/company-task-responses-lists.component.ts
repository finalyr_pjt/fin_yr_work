import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import AOS from 'aos';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-company-task-responses-lists',
  templateUrl: './company-task-responses-lists.component.html',
  styleUrls: ['./company-task-responses-lists.component.css']
})
export class CompanyTaskResponsesListsComponent implements OnInit {
  
  reqHeader = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + (localStorage.getItem('token'))
  });
  apiURL = environment.apiURL;
  user:any; 
  project_id: any;
  projectResponses: any;

  constructor(
    private router: Router,
    private location: Location,
    private http: HttpClient,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.user = localStorage.getItem('user');
    this.route.params.subscribe(params => {
      this.project_id = params['project_id'];
    });
    this.http.get(this.apiURL+'/dev/viewprojectresponses/'+this.project_id,{headers:this.reqHeader})
    .subscribe((projectResponses)=> {
      this.projectResponses=projectResponses;
      console.log(projectResponses)
    })
    
    AOS.init();

  }

  goback() {
    this.location.back();
  }

  grantedlists(){
    this.router.navigate(['/granted-points-list/'+this.project_id]);
  };

  viewtask(enrollment_id){
    this.router.navigate(['/task-response-view/'+this.project_id+'/'+enrollment_id]);
  }
  

}
