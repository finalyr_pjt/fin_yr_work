import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import AOS from 'aos';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-granted-responses-list',
  templateUrl: './granted-responses-list.component.html',
  styleUrls: ['./granted-responses-list.component.css']
})
export class GrantedResponsesListComponent implements OnInit {
  user: any;
  reqHeader = new HttpHeaders({ 
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + (localStorage.getItem('token'))
  });
  apiURL = environment.apiURL;
  grantedProjects: any;
  project_id: any;
  constructor(
    private router: Router,
    private location: Location,
    private http: HttpClient,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.user = localStorage.getItem('user');
    this.route.params.subscribe(params => {
      this.project_id = params['project_id'];
    });
    this.http.get(this.apiURL+'/dev/viewprojectgranted/'+this.project_id,{headers:this.reqHeader})
    .subscribe((grantedProjects)=> {
      this.grantedProjects=grantedProjects;
      console.log(grantedProjects);
    })
    AOS.init();
  }


  goback() {
    this.location.back();
  }

  grantedresponseview(enrollment_id){
    this.router.navigate(['./granted-response-view/'+this.project_id+'/'+enrollment_id]);
  };

}
