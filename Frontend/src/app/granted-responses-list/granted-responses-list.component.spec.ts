import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GrantedResponsesListComponent } from './granted-responses-list.component';

describe('GrantedResponsesListComponent', () => {
  let component: GrantedResponsesListComponent;
  let fixture: ComponentFixture<GrantedResponsesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GrantedResponsesListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GrantedResponsesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
