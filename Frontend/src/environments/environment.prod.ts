export const environment = {
  production: true,
  apiURL: 'https://crowd-sourcing-platform-webapp.herokuapp.com'
};
